const util = require('util');
const config = require('../configuration/config').config;
const s3_link_url = config.other.s3.base_url;
const users_photos_bucket = config.other.s3.users_photos_bucket;

const AWS = require('aws-sdk');
AWS.config.region = process.env.REGION;

const s3 = new AWS.S3({
    accessKeyId : config.other.s3.accessKeyId,
    secretAccessKey : process.env.S3_SECRET_KEY
});


// docs -> http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#deleteObjects-property
//todo Bucket names must be unique across all S3 users

exports.createPublicReadBucket = function(bucketName){
    return new Promise((resolve, reject) => {
        s3.createBucket({Bucket: bucketName, ACL : 'public-read'}, function(err, data) {
            if (err) {
                reject(err);
            }
            resolve(data);
        });
    });
    
};

exports.createPrivateBucket = function(bucketName){
    return new Promise((resolve, reject) => {
        s3.createBucket({Bucket: bucketName, ACL : 'private'}, function(err, data) {
            if (err) {
                reject(err);
            }
            resolve(data);
        });
    });    
};

exports.deleteBucket = function(bucketName){
    return new Promise((resolve, reject) => {
        s3.deleteBucket({Bucket : bucketName}, function(err, data){
            if(err){
                reject(err);
            }
            resolve(data);
        });
    });
};

exports.putPublicReadObjectIntoBucket = function(bucketName, object){
    return new Promise((resolve, reject) => {
        s3.putObject({
            Bucket : bucketName,
            Key : object.key,
            Body : object.value,
            ACL : "public-read"
        }, function(err, data) {
            if (err) {
                reject(err);
            }
            resolve(data);
        });
    });
};

exports.putPrivateObjectIntoBucket = function(bucketName, object) {
    return new Promise((resolve, reject) => {
        s3.putObject({
            Bucket: bucketName,
            Key: object.key,
            Body: object.value,
            ACL: "private"
        }, function (err, data) {
            if (err) {
                reject(err);
            }
            resolve(data);
        });
    });    
};

exports.deleteObjectFromBucket = function(bucketName, key){
    return new Promise((resolve, reject) => {
        s3.deleteObject({
            Bucket : bucketName,
            Key : key
        }, function(err, data){
            if(err){
                reject(err);
            }
            resolve(data);
        })
    });
};

exports.deleteObjectsFromBucket = function(bucketName, keysArr){
    var objects = [];

    keysArr.forEach(function(currentKey){
        objects.push({
            Key : currentKey
        });
    });

    return new Promise((resolve, reject) => {
        s3.deleteObjects({
            Bucket : bucketName,
            Delete : {
                Objects : objects,
                Quiet : false
            }
        }, function(err, data){
            if(err){
                reject(err);
            }
            resolve(data);
        });
    });    
};

exports.getBucketTagging = function(bucketName){
    return new Promise((resolve, reject) => {
        s3.getBucketTagging({Bucket : bucketName}, function(err, data){
            if(err){
                reject(err);
            }
            resolve(data);
        });
    });    
};

exports.getObjectFromBucket = function(bucketName, key){
    return new Promise((resolve, reject) => {
        s3.getObject({Bucket : bucketName, Key : key}, function(err, data){
            if(err){
                reject(err);
            }
            resolve(data);
        });
    });    
};

exports.getObjectAclFromBucket = function(bucketName, key){
    return new Promise((resolve, reject) => {
        s3.getObjectAcl({Bucket : bucketName, Key : key}, function(err, data){
            if(err){
                reject(err);
            }
            resolve(data);
        });
    });    
};

//returns simply data as a buffer
exports.getObjectTorrentFromBucket = function(bucketName, key){
    return new Promise((resolve, reject) => {
        s3.getObjectTorrent({Bucket : bucketName, Key : key}, function(err, data){
            if(err){
                reject(err);
            }
            resolve(data);
        });
    });   
};

//returns url to object but remember to remove from url substring after '?'
exports.getObjectUrlFromBucket = function(bucketName, key){
    return new Promise((resolve, reject) => {
        s3.getSignedUrl('putObject', {Bucket : bucketName, Key : key}, function(err, url){
            if(err){
                reject(err);
            }
            resolve(url);
        });
    });    
};

//below returns null if bucket exist otherwise returns error
exports.doesBucketExist = function(bucketName){
    return new Promise((resolve, reject) => {
        s3.headBucket({Bucket : bucketName}, function(err, data){
            if(err){
                reject(false);
            }
            resolve(true);
        });
    });    
};

exports.doesObjectExist = function(bucketName, key){
    return new Promise((resolve, reject) => {
        s3.headObject({Bucket : bucketName, Key : key}, function(err, data){
            if(err){
                reject(err);
            }
            resolve(data);
        });
    });   
};

exports.getBucketsList = function(){
    return new Promise((resolve, reject) => {
        s3.listBuckets(function(err, data){
            if(err){
                reject(err);
            }
            resolve(data);
        });
    });    
};

exports.getListOfObjectsWithinBucket = function(bucketName){
    return new Promise((resolve, reject) => {
        s3.listObjects({Bucket : bucketName},function(err, data){
            if(err){
                reject(err);
            }
            resolve(data);
        });
    });    
};

//todo should be correct ! below
exports.setPublicReadPermissionForBucket = function(bucketName){
    return new Promise((resolve, reject) => {
        s3.putBucketAcl({
            Bucket : bucketName,
            ACL : 'public-read'
        },function(err, data){
            if(err){
                reject(err);
            }
            resolve(true);
        });
    });    
};

exports.uploadObjectToBucketUsingStream = function(bucketName, key, stream){
    return new Promise((resolve, reject) => {
        s3.upload({Bucket : bucketName, Key : key, Body : stream}, function(err, data){
            if(err){
                reject(err);
            }
            resolve(data);
        });
    });    
};

exports.createUrlToObjectInBucket = function(bucketName, key){
    return s3_link_url + '/' + bucketName + '/' + key;
};

exports.extractPhotoIdFromURL = (url) => {
    return url.split('/').pop();
};

exports.s3 = s3;
exports.users_photos_bucket = users_photos_bucket;