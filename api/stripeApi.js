const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
stripe.setApiVersion('2018-02-28');
const config = require('../configuration/config').config;

exports.handleCharge = function(stripeToken, cashAmount, currency, description){
    return new Promise((resolve, reject) => {
        stripe.charges.create({
            amount: cashAmount,
            currency: currency,
            source: stripeToken,
            description: description,
            metadata: {}
        }, function(err, result){
            if(err){
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
};

// customer below
//todo 06.04.18 works
exports.createCustomer = async (email, description) => {
    const response = await stripe.customers.create({
        email,
        description
    });
    return response.id;
};

//todo 06.04.18 works
exports.getCustomerByIdentifier = (identifier) => {
    return stripe.customers.retrieve(identifier);
};

exports.doesCustomerExist = async (email) => {
    const customersList = await stripe.customers.list({
        email,
        limit : 1
    });

    return !!customersList.data.length;
};

//todo 06.04.18 works
exports.removeCustomer = (identifier) => {
    return stripe.customers.del(identifier);
};

//source below

//todo 06.04.18 works
//if customer has defined default source then it adds it as additional not default
exports.addSource = (customerIdentifier, sourceIdentifier) => {
    return stripe.customers.createSource(identifier, {
        source: sourceIdentifier
    });
};

//sets default_source
exports.setDefaultSource = (identifier, creditCardIdentifier) => {
    return stripe.customers.update(identifier, {
        default_source: creditCardIdentifier
    });
};

//todo 06.04.18 works
exports.getSource = (sourceIdentifier) => {
    return stripe.sources.retrieve(sourceIdentifier);
};

//todo 06.04.18 works -> returns removed source instead of operation success
exports.removeSource = (customerIdentifier, sourceIdentifier) => {
    return stripe.customers.deleteSource(customerIdentifier, sourceIdentifier);
};

//plan below
//todo 06.04.18 works
exports.createPlan = async (patternName, price, currency) => {
    const response = await stripe.plans.create({
        amount: price,
        interval: 'month',
        product : config.other.stripe.product_id, //it should be taken from config
        currency,
        nickname: patternName
    });

    return response.id;
};

//todo 07.04.18 works
exports.getPlan = (planIdentifier) => {
    return stripe.plans.retrieve(planIdentifier);
};

//todo 07.04.18 works
exports.removePlan = (planIdentifier) => {
    return stripe.plans.del(planIdentifier);
};

//subscription below

//todo works
//use when customer has not created any subscription before
exports.createSubscription = async (customerIdentifier, planIdentifier) => {
    const response = await stripe.subscriptions.create({
        customer: customerIdentifier,
        items: [
            {
                plan: planIdentifier
            }
        ]
    });

    return {
        subscriptionIdentifier: response.id,
        subscriptionItemIdentifier: response.items.data[0].id
    };
};

//todo works
exports.getSubscription = (subscriptionIdentifier) => {
    return stripe.subscriptions.retrieve(subscriptionIdentifier);
};


//todo works
// returns subscription item id
// plan cannot be add multiple times to subscription
exports.addPlanToSubscription = async (subscriptionIdentifier, planIdentifier) => {
    const response = await stripe.subscriptionItems.create({
        subscription: subscriptionIdentifier,
        plan: planIdentifier
    });

    return response.id;
};

//todo works
exports.removeSubscriptionItem = (subscriptionItemIdentifier) => {
    return stripe.subscriptionItems.del(subscriptionItemIdentifier);
};

//todo works
exports.removeSubscription = (subscriptionIdentifier) => {
    return stripe.subscriptions.del(subscriptionIdentifier,{
        //if set to true, subscription will be cancelled at the end of period
        at_period_end: true
    });
};

exports.getProductPlans = (productIdentifier) => {
    return stripe.plans.list({
        product: productIdentifier
    });
};

exports.getEvent = (eventIdentifier) => {
    return stripe.events.retrieve(eventIdentifier);
};

exports.stripe = stripe;
