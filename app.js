
require('dotenv').load();
// Include the cluster module
/*var cluster = require('cluster');

// Code to run if we're in the master process
if (cluster.isMaster) {

    // Count the machine's CPUs
    var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    // Listen for terminating workers
    cluster.on('exit', function (worker) {

        // Replace the terminated workers
        console.log('Worker ' + worker.id + ' died :(');
        cluster.fork();

    });

// Code to run if we're in a worker process
} else {*/

    const AWS = require('aws-sdk');
    const express = require('express');
    const bodyParser = require('body-parser');
    const path = require('path');
    const logger = require('morgan');
    const cors = require('cors');
    const util = require('util');
    const expressValidator = require('express-validator');
    const passport = require('passport');
    const config = require('./configuration/config').config;
    const cookieParser = require('cookie-parser');
    const cookieSession = require('cookie-session');
    const errorHandler = require('./errorHandler');

    let app = express();

    //database init
    const db = require('./database/relationalDB').db;

    //false means sync(force : false)
    db.initDb(false);


    //todo below init cache
    //const cache = require('./database/cache/cache');

    let server = require('http').Server(app);
    let io = require('socket.io')(server);

    const port = config.server.TCP_PORT || 3000;
    server.listen(port);
    console.log("server listens on "+port);

    //controllers
    //sockets controllers
    const mainSocketsController = require('./sockets/mainSocketsController').connectionHandler(io);

    //todo new index below
    const index = require('./controllers/index');
    const authController = require('./controllers/authController');
    const usersController = require('./controllers/usersController');
    const adminsController = require('./controllers/adminsController');
    const patternsController = require('./controllers/patternsController');
    const companiesDetailsController = require('./controllers/companiesDetailsController');
    const businessCardsController = require('./controllers/businessCardsController');
    const roomsController = require('./controllers/roomsController');
    const stripeWebhooksController =  require('./controllers/stripeWebhooksController');

    AWS.config.region = process.env.REGION;
    app.set('view engine', 'ejs');
    app.set('views', __dirname + '/views');

    app.use(logger('dev'));
    app.use(express.static(path.join(__dirname, 'public')));
    app.use('/stripe/webhooks', bodyParser.raw({type: '*/*'}));
    app.use('*', bodyParser.json({limit: '50mb'}));
    //app.use(bodyParser.json({limit: '50mb'}));
    app.use(cookieParser());
    app.use(cookieSession({
        keys: ['secret', 'keyboard cat']
    }));
    //below used for handling url-encoded requests
     app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use(cors());

    app.use(expressValidator());

    // Init passport
    app.use(passport.initialize());
    app.use(passport.session());
    require('./controllers/extraModules/passport')(passport);

    //controllers below
    app.use('/', index);
    app.use('/auth', authController);
    app.use('/users', usersController);
    app.use('/admins', adminsController);
    app.use('/patterns', patternsController);
    app.use('/companiesDetails', companiesDetailsController);
    app.use('/businessCards', businessCardsController);
    app.use('/rooms', roomsController);
    app.use('/stripe', stripeWebhooksController);

    // error handlers below

    // development error handler
    // will print stacktrace
    if (app.get('env') === 'development') {
        app.use((err, req, res, next) => {
            errorHandler.handleError(err, req, res, next);
        });
    }

    // production error handler
    // no stacktraces leaked to user
    app.use((err, req, res, next) => {
        errorHandler.handleError(err, req, res, next);
    });
//}