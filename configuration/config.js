require('dotenv').config();

const defaultConfig = {
    forgotPasswordTokenExpiry : 60*60*24*1000,
    jwt: {
        JWT_EXPIRATION_TIME : 60*60*24,
    },
    pictures: {
        profilePictureXdimension : 1000,
        profilePictureYdimension : 1000,
    }
};

const otherSettings = {
    development: {
        stripe : {
            product_id: 'prod_CdGrjxFW5Hilma',
            first_tier_plan: {
                identifier: 'plan_CfLOfSIBxYy6rr',
                minNumOfContacts: 1,
                maxNumOfContacts: 2
            },
            second_tier_plan: {
                identifier: 'plan_CfLXJBsbv0qLWw',
                minNumOfContacts: 3,
                maxNumOfContacts: 8
            },
            third_tier_plan: {
                identifier: 'plan_CfLXl1fvT6btWz',
                minNumOfContacts: 9
            },
            webhooks_secret: 'whsec_hgi3GdJIEBcApXgFxiXbZG3CPVSLyhu5'
        },
        s3 : {
            accessKeyId: 'AKIAJ7Y7SXYZF3M3V4CA',
            base_url: 'https://s3.eu-central-1.amazonaws.com/',
            users_photos_bucket: 'cardless-s3-bucket',
        }
    },
    test : {
        stripe : {
            product_id: 'prod_CdGrjxFW5Hilma',
            first_tier_plan: {
                identifier: 'plan_CfLOfSIBxYy6rr',
                minNumOfContacts: 30,
                maxNumOfContacts: 100
            },
            second_tier_plan: {
                identifier: 'plan_CfLXJBsbv0qLWw',
                minNumOfContacts: 101,
                maxNumOfContacts: 250
            },
            third_tier_plan: {
                identifier: 'plan_CfLXl1fvT6btWz',
                minNumOfContacts: 251
            },
            webhooks_secret: 'whsec_qBBLPybTP6U8WwyjvIzFK2fU1o7vlC6v'
        },
        s3 : {
            accessKeyId: 'awd',
            base_url: 'http://aws.s3.com'
        }
    },
    production : {
        stripe : {
            product_id: 'prod_CdGrjxFW5Hilma',
            first_tier_plan: {
                identifier: 'plan_CfLOfSIBxYy6rr',
                minNumOfContacts: 30,
                maxNumOfContacts: 100
            },
            second_tier_plan: {
                identifier: 'plan_CfLXJBsbv0qLWw',
                minNumOfContacts: 101,
                maxNumOfContacts: 250
            },
            third_tier_plan: {
                identifier: 'plan_CfLXl1fvT6btWz',
                minNumOfContacts: 251
            },
            webhooks_secret: 'whsec_qBBLPybTP6U8WwyjvIzFK2fU1o7vlC6v'
        },
        s3 : {
            accessKeyId: 'awd',
            base_url: 'http://aws.s3.com'
        }
    },
};

const server = {
    development: {
        domain : 'http://localhost:3001/',
        REDIS_URL: 'localhost',
        REDIS_PORT: '6379',
        SERVER_URL: 'localhost',
        TCP_PORT: 3001
    },
    test: {
        domain : 'http://test:3001/',
        REDIS_URL: 'localhost',
        REDIS_PORT: '6379',
        SERVER_URL: 'localhost',
        TCP_PORT: 3001
    },
    production: {
        domain : 'http://production:3001/',
        REDIS_URL: 'localhost',
        REDIS_PORT: '6379',
        SERVER_URL: 'localhost',
        TCP_PORT: 3001
    }
};

const database = {
    development: {
        username: 'root',
        password: 't34pt91',
        database: 'cardless_db',
        host: 'localhost',
        dialect: 'mysql',
        db_port: 3306,
    },
    test: {
        username: 'test',
        password: 'test',
        database: 'cardless_db',
        host: 'localhost',
        dialect: 'mysql',
        db_port: 3306,
    },
    production: {
        username: 'root',
        password: 't34pt91',
        database: 'cardless_db',
        host: 'localhost',
        dialect: 'mysql',
        db_port: 3306,
    }
};

console.log("env in config obj construction = ", process.env.NODE_ENV);

let configToBeExport = { ...defaultConfig };

switch(process.env.NODE_ENV){
    case 'development':
        configToBeExport['other'] = { ...otherSettings.development };
        configToBeExport['server'] = { ...server.development };
        configToBeExport['database'] = { ...database.development };
        break;
    case 'test':
        configToBeExport['other'] = { ...otherSettings.test };
        configToBeExport['server'] = { ...server.test };
        configToBeExport['database'] = { ...database.test };
        break;
    case 'production':
        configToBeExport['other'] = { ...otherSettings.production };
        configToBeExport['server'] = { ...server.production };
        configToBeExport['database'] = { ...database.production };
        break;
}

module.exports = {
    config: {...configToBeExport},
    ...database
};