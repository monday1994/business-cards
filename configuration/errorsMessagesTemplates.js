const invalidTokenErrorMessage = 'Access token is not valid';
const tooMuchFilesSent = 'You\'ve sent too much files';
module.exports = {
    INVALID_TOKEN_ERROR_MESSAGE : invalidTokenErrorMessage,
    TOO_MUCH_FILES_MESSAGE : tooMuchFilesSent
};