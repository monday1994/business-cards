const path = require('path'),
      config = require('./config').config;

module.exports = {
    pathToUsersImagesDirectory : path.normalize(__dirname+'/../files/usersImages'),
    linkToProfilePhoto : config.server.domain+'users/getProfilePhoto',
};