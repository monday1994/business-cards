const express = require('express');
let router = express.Router();
const util = require('util');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const usersRepo = require('../database/repositories/usersRepo');
const adminsRepo = require('../database/repositories/adminsRepo');
const requestValidator = require('./requestsValidators/adminsRequestsValidator');
const crypto = require('../controllers/extraModules/crypto');
const shortId = require('shortid');
const sequelize = require('../database/relationalDB').sequelize;
const EMT = require('../configuration/errorsMessagesTemplates');
const sendgridApi = require('../api/sendgridApi');
const cryptoRandomString = require('crypto-random-string');
/*
    headers : {
        Content-Type : application/json,
        Authorization : admin JWT accessToken
    }
    body : {
        name : 'user'
        lastname : 'userlastname'
        email : 'test@gmail.com',
        password : 'somepass',
        phone : '515-689-233'
        location : 'cracow'
    }
*/
//todo should work 29.11.17
router.post('/createAdmin', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    if(req.user.role === 'admin'){
        requestValidator.validateCreateAdmin(req).then(result =>{

            let newAdmin = {
                name : req.body.name,
                lastname : req.body.lastname,
                email : req.body.email,
                phone : req.body.phone,
                location : req.body.location
            };

            let password = shortId.generate();

            let iv_string = cryptoRandomString(16);

            crypto.encrypt(password, iv_string).then(encryptedPassword => {
                newAdmin.password = encryptedPassword;
                newAdmin.iv_string = iv_string;

                adminsRepo.create(newAdmin).then((createdAdmin) => {
                    delete createdAdmin.get({plain : true}).password;

                    sendgridApi.sendEmailWithPasswordToNewAdmin(newAdmin.name, newAdmin.lastname, password, newAdmin.email).then(result => {
                        res.json({
                            createdAdmin : createdAdmin
                        });
                    }).catch(err =>{
                        return next({status : 400, error : err});
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            }).catch(err =>{
                return next({
                    status : 400,
                    error : err
                });
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    } else {
        return next({status : 401, error : EMT.INVALID_TOKEN_ERROR_MESSAGE});
    }
});

//todo remove admin
/*
    headers : {
        Content-Type : application/json,
        Authorization : admin JWT accessToken
    }
    body : {
        adminId : 1
    }
*/
//todo works properly 30.04.18
router.post('/removeAdmin', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    if(req.user.role === 'admin'){
        requestValidator.validateRemoveAdmin(req).then(result =>{
            let { adminId } = req.body;

            adminsRepo.removeById(adminId).then(result => {
                 res.json({
                     result: result ? 'success' : 'failure'
                 });
            }).catch(err => {
                return next({status: 400, error : err});
            })

        }).catch(err => {
            return next({status : 400, error : err});
        });
    } else {
        return next({status : 401, error : EMT.INVALID_TOKEN_ERROR_MESSAGE});
    }
});

module.exports = router;