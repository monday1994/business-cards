'use strict';
const express = require('express');
let router = express.Router();
const util = require('util');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const usersRepo = require('../database/repositories/usersRepo');
const usersAuthsRepo = require('../database/repositories/usersAuthsRepo');
const stripeCustomersRepo = require('../database/repositories/stripeCustomersRepo');
const adminsRepo = require('../database/repositories/adminsRepo');
const EMT = require('../configuration/errorsMessagesTemplates');
const JWT_AUTH_SECRET = process.env.JWT_AUTH_SECRET;
const JWT_EXPIRATION_TIME = require('../configuration/config').config.jwt.JWT_EXPIRATION_TIME;
const config = require('../configuration/config').config;
const crypto = require('../controllers/extraModules/crypto');
const shortid = require('shortid');
const stripeApi = require('../api/stripeApi');
const UIDGenerator = require('uid-generator');
const uidgen = new UIDGenerator();
const sendgridApi = require('../api/sendgridApi');
const paths = require('../configuration/paths');
const fs = require('fs');
const requestValidator = require('./requestsValidators/authRequestsValidator');
const cryptoRandomString = require('crypto-random-string');
const Promise = require('bluebird');
const sequelize = require('../database/relationalDB').sequelize;

/*
    headers : {
        Content-Type : application/json,
    }
    body : {
        name : 'francis',
        lastname : 'monday',
        email : 'test@gmail.com'
    }

    returns -> new user, iv_string
*/
//todo 05.04.18 works, transactions added
router.post('/registration', (req,res,next) => {
    "use strict";

    let t;

    requestValidator.validateRegistration(req).then(async result =>{

        let newUser = {
            name : req.body.name,
            lastname : req.body.lastname,
            email : req.body.email,
            linkedinEmail : null,
            linkToProfilePhoto : null
        };

        let newAuth = {
            externalId: null,
            email: req.body.email,
            forgotPasswordToken : null,
            forgotPasswordTokenExpiry : null,
            type : 'STANDARD'
        };

        let password = shortid.generate();

        let iv_string = cryptoRandomString(16);

        newAuth.password = await crypto.encrypt(password, iv_string);
        newAuth.iv_string = iv_string;

        const doesCustomerAlreadyExist = await stripeApi.doesCustomerExist(newUser.email);

        if(doesCustomerAlreadyExist){
            return next({status : 409, error: `User with mail : ${newUser.email} already exist`});
        }

        const customerIdentifier = await stripeApi.createCustomer(newUser.email, `${newUser.name} ${newUser.lastname}`);

        const newCustomer = {
            customerIdentifier,
            isBlocked: false,
            subscriptionItems: [],
        };

        t = await sequelize.transaction();

        const createdStripeCustomer = await stripeCustomersRepo.create(newCustomer, t);

        const user = await usersRepo.create(newUser, t);
        const auth = await usersAuthsRepo.create(newAuth, t);
        await user.addUserAuths(auth, { transaction: t });
        await createdStripeCustomer.setUser(user, { transaction: t });

        let plainUser = user.get({plain : true});
        //todo enable sendgrid
        //await sendgridApi.sendEmailWithPasswordToNewUser(plainUser.name, plainUser.lastname, password, plainUser.email);

        await t.commit();

        res.json({
            newUser : plainUser,
            iv_string : iv_string
        });
    }).catch(async err => {

        if(t){
            await t.rollback();
        }

        return next({ status : 400, error : err});
    });
});



/*
    headers : {
        Content-Type : application/json,
    }
    body : {
        name : 'francis',
        lastname : 'monday',
        linkedinEmail : 'test@gmail.com',
        linkedinId: '12312312313123',
    }

    returns -> new user
*/
//todo 07.04.18 works
//todo stripe customer creation added, transactions added
//todo enable sendgrid
router.post('/linkedinRegistration', (req,res,next) => {
    "use strict";

    let t;

    requestValidator.validateLinkedinRegistration(req).then(async result =>{

        let newUser = {
            name : req.body.name,
            lastname : req.body.lastname,
            email : null,
            linkedinEmail : req.body.linkedinEmail,
            linkToProfilePhoto : null
        };

        let newAuth = {
            externalId: req.body.linkedinId,
            email: req.body.linkedinEmail,
            forgotPasswordToken : null,
            forgotPasswordTokenExpiry : null,
            type : 'LINKEDIN'
        };

        let password = shortid.generate();

        let iv_string = cryptoRandomString(16);

            newAuth.password = await crypto.encrypt(password, iv_string);
            newAuth.iv_string = iv_string;

            const doesCustomerAlreadyExist = await stripeApi.doesCustomerExist(newUser.linkedinEmail);

            if(doesCustomerAlreadyExist){
                return next({status : 409, error: `User with mail : ${newUser.linkedinEmail} already exist`});
            }

            t = await sequelize.transaction();
            //createLinkedinAccount handles user, auth and customer entities creation (also stripe customer account)
            let { createdUser, createdAuth, createdCustomer } = await usersAuthsRepo.createLinkedinAccount(newAuth, newUser, t);

            await Promise.all([
                createdUser.addUserAuths(createdAuth, {transaction: t}),
                createdCustomer.setUser(createdUser, {transaction: t})
            ]);

            let plainUser = createdUser.get({plain : true});
            //todo enable sendgrid
            //await sendgridApi.sendEmailToNewUser(plainUser.name, plainUser.lastname, plainUser.email);

            await t.commit();
            res.json({
                newUser : plainUser,
                iv_string : iv_string,
                password : password
            });
    }).catch(async err => {
        if(t){
            await t.rollback();
        }

        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json,
    }
    body : {
        userId : 1,
        linkedinEmail : 'test@gmail.com',
        linkedinId: '12312312313123',
    }

    returns -> new user
*/
//todo 05.04.18 works, tranasction added
router.post('/addLinkedinAuth', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    let t;

    requestValidator.validateAddLinkedinAuth(req).then(async result =>{

        let userToBeUpdate = {
            id : req.body.userId,
            linkedinEmail : req.body.linkedinEmail
        };

        let newAuth = {
            externalId: req.body.linkedinId,
            email: req.body.linkedinEmail,
            forgotPasswordToken : null,
            forgotPasswordTokenExpiry : null,
            type : 'LINKEDIN'
        };

        let password = shortid.generate();

        let iv_string = cryptoRandomString(16);

        newAuth.password = await crypto.encrypt(password, iv_string);
        newAuth.iv_string = iv_string;

        t = await sequelize.transaction();

        await usersAuthsRepo.addLinkedinAccount(newAuth, userToBeUpdate, t);
            await t.commit();
            res.json({
                iv_string : iv_string,
                password : password
            });
    }).catch(async err => {
        if(t){
            await t.rollback();
        }
        return next({status : 400, error : err});
    });
});


/*
    headers : {
        Content-Type : application/json,
    }
    body : {
        email : 'test@gmail.com',
        password : 'somePass'
    }

    returns -> token, user role and id
*/
//todo 05.04.18 works
router.post('/userLogin', (req,res,next) => {
    "use strict";

    requestValidator.validateLogin(req).then(result =>{
        let email = req.body.email;
        let password = req.body.password;

        usersAuthsRepo.validateLogin(email, password).then(userAuth => {

            const dataForSign = {
                id : userAuth.UserId,
                email : userAuth.email,
                role : 'user',
                type : userAuth.type
            };
            jwt.sign(dataForSign, JWT_AUTH_SECRET,{ expiresIn: JWT_EXPIRATION_TIME }, (err, token) => {
                if(err){
                    return next({
                        status : 400,
                        error : err
                    });
                }
                res.json({
                    id : userAuth.UserId,
                    token : token,
                    role : 'user'
                });
            });
        }).catch(err => {
            return next({ status : 400, error : err});
        })
    }).catch(err => {
        return next({status : 400, error : err});
    });
});


/*
    headers : {
        Content-Type : application/json,
    }
    body : {
        email : 'test@gmail.com',
        password : 'somePass'
    }

    returns -> token, user role and id
*/
//todo 06.04.18 works
router.post('/adminLogin', (req,res,next) => {
    "use strict";

    requestValidator.validateLogin(req).then(result =>{
        let email = req.body.email;
        let password = req.body.password;

        adminsRepo.validateLogin(email, password).then(admin => {
            delete admin.password;

            const dataForSign = {
                id : admin.id,
                email : admin.email,
                role : 'admin'
            };

            jwt.sign(dataForSign, JWT_AUTH_SECRET,{ expiresIn: JWT_EXPIRATION_TIME }, (err, token) => {
                if(err){
                    return next({
                        status : 400,
                        error : err
                    });
                }
                res.json({
                    id : admin.id,
                    token : token,
                    role : 'admin'
                });
            });
        }).catch(err => {
            return next({ status : 400, error : err});
        })
    }).catch(err => {
        return next({status : 400, error : err});
    });
});



/*
    headers : {
        Content-Type : application/json,
        Authorization : JWT accessToken
    }
    //both passwords should be encrypted
    body : {
        userId : 1,
        userEmail : 'user@gmail.com',
        oldPassword : 'some pass',
        newPassword : 'new pass',
        iv_string : '16 characters long'
    }
*/

//todo 05.04.18 works
router.post('/changeUserPassword', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateChangePassword(req).then(result =>{

        let oldPassword = req.body.oldPassword;
        let newPassword = req.body.newPassword;
        let iv_string = req.body.iv_string;
        let userId = req.body.userId;
        let userEmail = req.body.userEmail;

        Promise.all([
            usersAuthsRepo.validateOldPassword(userEmail, oldPassword),
            usersRepo.getById(userId)
        ]).spread((result, user)=>{

            crypto.decrypt(newPassword, iv_string).then(decryptedNewPassword => {
                let userAuthToBeUpdate = {
                    UserId : userId,
                    password : newPassword,
                    iv_string: iv_string,
                    type: 'STANDARD'
                };

                usersAuthsRepo.update(userAuthToBeUpdate).then(updatedAuth => {
                    let plainAuth = updatedAuth.get({plain : true});
                    //sendgridApi.sendEmailWithChangedPassword(user.name, user.lastname, decryptedNewPassword, plainAuth.email).then(result => {
                    res.json({
                        message : 'user password has been changed'
                    });
                    /* }).catch(err =>{
                         return next({status : 400, error : err});
                     });*/
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            }).catch(err => {
                return next({status : 400, error : err});
            });
        }).catch(err => {
            return next({status : 400, error: err});
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : admin JWT accessToken
    }
    body : {
        //both encrypted
        oldPassword : 'some pass',
        newPassword : 'new pass',
        iv_string : '16 characters long iv string'
    }
*/
//todo 06.04.18 works
router.post('/changeAdminPassword', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";
    requestValidator.validateChangePassword(req).then(result =>{

        if(req.user.role === 'admin'){
            let oldPassword = req.body.oldPassword;
            let newPassword = req.body.newPassword;
            let iv_string = req.body.iv_string;

            let adminEmail = req.user.email;
            adminsRepo.validateOldPassword(adminEmail, oldPassword).then(admin => {

                crypto.decrypt(newPassword, iv_string).then(decryptedNewPassword => {
                    let adminToBeUpdate = {
                        id : req.user.id,
                        password : newPassword,
                        iv_string: iv_string,
                    };

                    adminsRepo.update(adminToBeUpdate).then(updatedAdmin => {
                        let plainAdmin = updatedAdmin.get({plain:true});
                        sendgridApi.sendEmailWithChangedPassword(plainAdmin.name, plainAdmin.lastname, decryptedNewPassword, plainAdmin.email).then(result => {
                            res.json({
                                message : 'admin password has been changed'
                            });
                        }).catch(err =>{
                            return next({status : 400, error : err});
                        });
                    }).catch(err => {
                        return next({status : 400, error : err});
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            }).catch(err => {
                return next({status : 400, error : err});
            });
        } else {
            return next({
                status: 401,
                error : EMT.INVALID_TOKEN_ERROR_MESSAGE
            })
        }
    }).catch(err => {
        return next({status : 400, error : err});
    });
});


/*
    headers : {
        Content-Type : application/json
    }
    body : {
        email : 'user@gmail.com'
    }
*/
//todo 06.04.18 works, transaction implemented
router.post('/forgotUserPassword', (req,res,next) => {
    "use strict";

    let t;

    requestValidator.validateForgotPassword(req).then(result => {
        let email = req.body.email;

        Promise.all([
            usersRepo.getByEmail(email),
            usersAuthsRepo.getStandardAuthByEmail(email)
        ]).spread(async (user, userAuth) => {

            const generatedToken = await uidgen.generate();

            userAuth.forgotPasswordToken = generatedToken;
            userAuth.forgotPasswordTokenExpiry = (new Date().getTime() + Number(config.forgotPasswordTokenExpiry));

            let forgotPasswordLink = `${config.server.domain}auth/resetUserPassword/${generatedToken}`;

            let plainUser = user.get({plain: true});
            let plainUserAuth = userAuth.get({plain: true});

            t = await sequelize.transaction();

            await usersAuthsRepo.tUpdate(plainUserAuth, t);
            //await sendgridApi.sendEmailWithResetPasswordLink(plainUser.name, forgotPasswordLink, plainUser.email);
            await t.commit();
            res.json({
                message: 'reset link has been sent via email'
            });
        }).catch(async err => {
            if (t) {
                await t.rollback();
            }
            return next({status: 400, error: err});
        });
    });
});

/*
    headers : {

    }
    params : {
        token : 'wdawdj12949813dfahwd78y1278'
    }
*/
//todo 06.04.18 works, enable sendgrid, transaction added
router.get('/resetUserPassword/:token', (req,res,next) => {
    let t;

    requestValidator.validateResetPasswordToken(req).then(async result => {
        let token = req.params.token;

        const userAuth = await usersAuthsRepo.getByForgotPasswordToken(token);

        let password = shortid.generate();

        let iv_string = cryptoRandomString(16);


        userAuth.password = await crypto.encrypt(password, iv_string);
        userAuth.iv_string = iv_string;
        userAuth.forgotPasswordTokenExpiry = null;
        userAuth.forgotPasswordToken = null;

        let plainUserAuth = userAuth.get({plain : true});

        t = await sequelize.transaction();

        const user = await usersRepo.getById(plainUserAuth.UserId);
        await usersAuthsRepo.tUpdate(plainUserAuth, t);
        //await sendgridApi.sendEmailAfterResetPassword(user.name, password, plainUserAuth.email);

        await t.commit();

        res.json({
            message : 'new password has been sent via email'
        });
    }).catch(async err =>{
        if(t){
            await t.rollback();
        }

        return next({status : 400, error : err});
    });
});


/*
    headers : {
        Content-Type : application/json
    }
    body : {
        email : 'user@gmail.com'
    }
*/
//todo 06.04.18 works
router.post('/forgotAdminPassword', (req,res,next) => {
    "use strict";
    requestValidator.validateForgotPassword(req).then(result =>{
        let email = req.body.email;

        adminsRepo.getByEmail(email).then(admin => {
            uidgen.generate().then(generatedToken => {
                admin.forgotPasswordToken = generatedToken;
                admin.forgotPasswordTokenExpiry = (new Date().getTime()+Number(config.forgotPasswordTokenExpiry));

                let forgotPasswordLink = `${config.server.domain}auth/resetAdminPassword/${generatedToken}`;

                let plainAdmin = admin.get({plain:true});

                adminsRepo.update(plainAdmin).then(updatedAdmin => {
                    sendgridApi.sendEmailWithResetPasswordLink(plainAdmin.name, forgotPasswordLink, plainAdmin.email).then(result => {
                        res.json({
                            message : 'reset link has been sent via email'
                        });
                    }).catch(err => {
                        return next({status : 400, error : err});
                    });
                }).catch(err =>{
                    return next({status : 400, error : err});
                });
            }).catch(err => {
                return next({
                    status : 400,
                    error : err
                });
            });
        }).catch(err => {
            return next({
                status : 400,
                error : err
            });
        })
    }).catch(err => {
        return next({status : 400, error : err});
    });
});


/*
    headers : {

    }
    params : {
        token : 'wdawdj12949813dfahwd78y1278'
    }
*/
//todo 06.04.18 works
router.get('/resetAdminPassword/:token', (req,res,next) => {
    requestValidator.validateResetPasswordToken(req).then(result => {
        let token = req.params.token;

        adminsRepo.getByForgotPasswordToken(token).then(admin => {

            let password = shortid.generate();

            let iv_string = cryptoRandomString(16);

            crypto.encrypt(password, iv_string).then(encryptedPassword => {
                admin.password = encryptedPassword;
                admin.iv_string = iv_string;

                admin.forgotPasswordTokenExpiry = null;
                admin.forgotPasswordToken = null;

                let plainAdmin = admin.get({plain : true});

                adminsRepo.update(plainAdmin).then(updatedAdmin => {
                    sendgridApi.sendEmailAfterResetPassword(plainAdmin.name, password, plainAdmin.email) .then(result => {
                        res.json({
                            message : 'new password has been sent via email'
                        });
                    }).catch(err =>{
                        return next({status : 400, error : err});
                    });
                }).catch(err => {
                    return next({ status : 400, error : err});
                })
            }).catch(err=>{
                return next({ status : 400, error : err })
            });
        }).catch(err => {
            return next({status : 400, error : err});
        })
    }).catch(err =>{
        return next({status : 400, error : err});
    });
});



module.exports = router;