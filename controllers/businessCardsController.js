const express = require('express');
let router = express.Router();
const util = require('util');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const businessCardsRepo = require('../database/repositories/businessCardsRepo');
const companiesDetailsRepo = require('../database/repositories/companiesDetailsRepo');
const patternsRepo = require('../database/repositories/patternsRepo');
const usersRepo = require('../database/repositories/usersRepo');
const requestValidator = require('./requestsValidators/businessCardsRequestsValidator');
const crypto = require('../controllers/extraModules/crypto');
const shortId = require('shortid');
const sequelize = require('../database/relationalDB').sequelize;
const EMT = require('../configuration/errorsMessagesTemplates');
const sendgridApi = require('../api/sendgridApi');
const multerFilesValidator = require('./requestsValidators/multerFilesValidator');
const config = require('../configuration/config').config;
const Promise = require('bluebird');
const async = require('async');
const _ = require('lodash');
const stripeApi = require('../api/stripeApi');
const stripeCustomersRepo = require('../database/repositories/stripeCustomersRepo');
const stripeSubscriptionHandler = require('./extraModules/stripeSubscriptionHandler');

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        userId : 1,
        colors : JSON({color : #123123, fullfilment : 30})
        CompanyDetailsId : 1
    }
*/
//todo 06.04.18 works perfectly, transactions added
router.post('/createBusinessCard', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";
    let t;

    requestValidator.validateCreateBusinessCard(req).then(async result => {

        let userId = req.body.userId;

        let newBusinessCard = {
            colors : JSON.stringify(req.body.colors),
            CompanyDetailsId : req.body.CompanyDetailsId,
            UserId : userId
        };

        t = await sequelize.transaction();

        const [user, companyDetails ] = await Promise.all([
            usersRepo.getById(userId),
            companiesDetailsRepo.getById(newBusinessCard.CompanyDetailsId)
        ]);

        const createdBusinessCard = await businessCardsRepo.create(newBusinessCard, t);
        await Promise.all([
            createdBusinessCard.setCompanyDetails(companyDetails, {transaction: t}),
            user.addBusinessCard(createdBusinessCard, {transaction: t})
        ]);

        await t.commit();

        res.json({
            createdBusinessCard : createdBusinessCard
        });

    }).catch(async err => {
        if(t){
            await t.rollback();
        }
        return next({status : 400, error : err});
    });
});


/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        id : 1,
        colors : JSON({color : #123123, fullfilment : 30})
        userId : 1
    }
*/
//todo 06.04.18 works
router.post('/updateBusinessCard', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateUpdateBusinessCard(req).then(result => {

        let businessCardToBeUpdate = {
            id : req.body.id,
            colors : JSON.stringify(req.body.colors),
            UserId : req.body.userId
        };

        businessCardsRepo.update(businessCardToBeUpdate).then(updatedBusinessCard => {
            res.json({
                updatedBusinessCard : updatedBusinessCard
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});



/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        userId : 1,
        businessCardId : 1,
        patternId : 2
    }
*/

//todo 06.04.18 works, transaction added
router.post('/setFreePattern', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    let t;

    requestValidator.validateSetPattern(req).then(async result => {
        const { businessCardId, patternId, userId } = req.body;

        const [ businessCard, rentedPatterns, pattern, stripeCustomer ] = await Promise.all([
            businessCardsRepo.getById(businessCardId),
            usersRepo.getUserWithPatterns(userId),
            patternsRepo.getById(patternId),
            stripeCustomersRepo.getByUserId(userId)
        ]);

        const plainStripeCustomer = stripeCustomer.get({plain:true});

        if(businessCard.UserId === userId){
            let hasUserAlreadyRentPattern = _.find(rentedPatterns.Patterns, {id : patternId});

            t = await sequelize.transaction();

            if(hasUserAlreadyRentPattern && pattern.price === 0){
                // user is setting already rented pattern or pattern is free then no need to subscribe this again

                //check if associated pattern should be unsubscribed
                if(businessCard.PatternId){
                    console.log("has old pattern to be unsubscribe ");

                    const numberOfCardsAssociatedWithPattern = await businessCardsRepo.getNumberOfUserCardsAssociatedWithPattern(userId, businessCard.PatternId);
                    console.log("numberOfCardsAssociated = ", numberOfCardsAssociatedWithPattern);

                    // if former current BC pattern is being used in other BC's then it cannot be unsubscribed
                    if(Number(numberOfCardsAssociatedWithPattern) === 1){
                        await stripeSubscriptionHandler.unsubscribeOldPattern(businessCard, plainStripeCustomer, rentedPatterns, t);
                    }
                }

                await businessCard.setPattern(pattern, {transaction: t});

                await t.commit();

                res.json({
                    message : 'Pattern with id : '+patternId+' has been associated with business card - id : '+businessCardId
                });
            } else if(pattern.price === 0) {

                //check if associated pattern should be unsubscribed
                if(businessCard.PatternId){
                    console.log("has old pattern to be unsubscribe ");

                    const numberOfCardsAssociatedWithPattern = await businessCardsRepo.getNumberOfUserCardsAssociatedWithPattern(userId, businessCard.PatternId);
                    console.log("numberOfCardsAssociated = ", numberOfCardsAssociatedWithPattern);

                    // if former current BC pattern is being used in other BC's then it cannot be unsubscribed
                    if(Number(numberOfCardsAssociatedWithPattern) === 1){
                        await stripeSubscriptionHandler.unsubscribeOldPattern(businessCard, plainStripeCustomer, rentedPatterns, t);
                    }
                }

                await rentedPatterns.addPatterns(pattern, {transaction: t});
                await businessCard.setPattern(pattern, {transaction: t});

                await t.commit();

                res.json({
                    message : 'Pattern with id : '+patternId+' has been associated with business card - id : '+businessCardId
                });
            } else {
                return next({status: 400, error: `Chosen pattern is not free`});
            }
        } else {
            return next({status : 400, error : `User with id : ${userId} is not owner of business card with id : ${businessCard.id}`});
        }
    }).catch(async err => {
        if(t){
            await t.rollback();
        }
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        userId : 1,
        businessCardId : 1,
        patternId : 2
    }
*/

//const jsParser = require('body-parser').json();
//todo 11.04.18 works properly, transactions added
router.post('/setPattern', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    let t;

    requestValidator.validateSetPattern(req).then(async result => {
        const { businessCardId, patternId, userId } = req.body;

        const [ businessCard, rentedPatterns, pattern, stripeCustomer ] = await Promise.all([
            businessCardsRepo.getById(businessCardId),
            usersRepo.getUserWithPatterns(userId),
            patternsRepo.getById(patternId),
            stripeCustomersRepo.getByUserIdIfUserHasCreditCard(userId)
        ]);

        const plainStripeCustomer = stripeCustomer.get({plain:true});

        if(businessCard.UserId === userId && pattern.price > 0){

            t = await sequelize.transaction();

            const hasUserAlreadyRentPattern = _.find(rentedPatterns.Patterns, {id : patternId});

            if(hasUserAlreadyRentPattern){
                // user is setting already rented pattern then no need to subscribe this again
                if(businessCard.PatternId){
                    console.log("has old pattern to be unsubscribe ");

                    const numberOfCardsAssociatedWithPattern = await businessCardsRepo.getNumberOfUserCardsAssociatedWithPattern(userId, businessCard.PatternId);
                    console.log("numberOfCardsAssociated = ", numberOfCardsAssociatedWithPattern);

                    // if former current BC pattern is being used in other BC's then it cannot be unsubscribed
                    if(Number(numberOfCardsAssociatedWithPattern) === 1){
                        //unsubscribe old
                        console.log("unsubscribing old");
                        await stripeSubscriptionHandler.unsubscribeOldPattern(businessCard, plainStripeCustomer, rentedPatterns, t);
                    }
                }

                await businessCard.setPattern(pattern, {transaction: t});

                await t.commit();
                res.json({
                    message : 'Pattern with id : '+patternId+' has been associated with business card - id : '+businessCardId
                });
            } else {
                if(businessCard.PatternId){
                    console.log("has old pattern to be unsubscribe ");

                    const numberOfCardsAssociatedWithPattern = await businessCardsRepo.getNumberOfUserCardsAssociatedWithPattern(userId, businessCard.PatternId);
                    console.log("numberOfCardsAssociated = ", numberOfCardsAssociatedWithPattern);

                    // if former current BC pattern is being used in other BC's then it cannot be unsubscribed
                    if(Number(numberOfCardsAssociatedWithPattern) === 1){
                        //unsubscribe old
                        console.log("unsubscribing old");
                        await stripeSubscriptionHandler.unsubscribeOldPattern(businessCard, plainStripeCustomer, rentedPatterns, t);
                    }

                    await stripeSubscriptionHandler.subscribeNewPattern(plainStripeCustomer, pattern, t);
                } else {
                    console.log("does not have old pattern to unsubscribe");
                    await stripeSubscriptionHandler.subscribeNewPattern(plainStripeCustomer, pattern, t);

                }
                await rentedPatterns.addPatterns(pattern, {transaction: t});
                await businessCard.setPattern(pattern, {transaction: t});

                await t.commit();
                res.json({
                    message : 'Pattern with id : '+patternId+' has been associated with business card - id : '+businessCardId
                });
            }
        } else {
            return next({status : 400, error : `User with id : ${userId} is not owner of business card with id : ${businessCard.id} or pattern is for free`});
        }
    }).catch(async err => {
        if(t){
            await t.rollback();
        }
        return next({status : 400, error : err});
    });
});


/*
    headers : {
        Authorization : user JWT accessToken
    }
    query : {
        userId: 1,
        id : 1
    }
*/
//todo works properly 28.12.17
router.get('/getBusinessCardContactsIds', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    requestValidator.validateGetBusinessCardContacts(req).then(result => {
        let id = req.query.id;
        let userId = req.query.userId;

        businessCardsRepo.getCardWithContactsById(id).then(businessCard => {
            let plainCard = businessCard.get({plain : true});
            let arrOfContacts = plainCard.Contacts;
            let contactsToBeReturn = [];

            async.each(arrOfContacts, (currentContact, cb) => {
                contactsToBeReturn.push({
                    userId : currentContact.UserId,
                    contactBusinessCardId : currentContact.CardCard.ContactId
                });
                cb(null);
            }, err => {
                if(err){
                    return next({status : 400, error : err});
                } else {
                    if(businessCard.UserId === userId){
                        res.json({
                            contacts : contactsToBeReturn
                        });
                    } else {
                        return next({status : 401, error : 'Only card owner can obtain list of contacts belongs to given card'});
                    }
                }
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err =>{
        return next({status : 400, error : err});
    })
});

/*
    headers : {
        Authorization : user JWT accessToken
    }
    query : {
        id : 1
        userId : 2,
    }
*/
//todo works properly 06.04.18
router.get('/getBusinessCardById', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    requestValidator.validateGetBusinessCardById(req).then(result => {
        let id = req.query.id;
        let userId = req.query.userId;

        businessCardsRepo.getByIdWithAssociatedPatternAndCompanyDetails(id).then(businessCard => {
            if(businessCard.UserId === userId){
                res.json({
                    businessCard : businessCard
                });
            } else {
                usersRepo.getUserContacts(businessCard.UserId).then(arrOfContacts => {

                    let doesUserIsInContacts = _.find(arrOfContacts, { UserId : userId});

                    if(doesUserIsInContacts){
                        res.json({
                            businessCard : businessCard
                        });
                    } else {
                        return next({status : 401, error : 'User with id : '+userId+' does not have access to card with id : '+id});
                    }
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            }
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err =>{
        return next({status : 400, error : err});
    })
});

/*
    headers : {
        Content-Type : application/json
        Authorization : user JWT accessToken
    }
    body : {
        userId : 1,
        id : 1
    }
*/
//todo 16.04.18 works properly stripe and TRANSACTIONS implemented. LOOK AT ISSUE BEFORE SET OR UNSET SUBSCRIPTION
router.post('/deleteById', passport.authenticate('jwt', {session: false}), (req,res,next) => {

    let t,
        t2;

    requestValidator.validateDeleteById(req).then(async result => {
        let { id, userId } = req.body;

        const businessCardToBeRemove = await businessCardsRepo.getByIdWithCompanyDetailsAndPattern(id);
        const plainBusinessCard = businessCardToBeRemove.get({plain:true});
        if(plainBusinessCard.UserId === userId){

            t = await sequelize.transaction();

            const numberOfCardsAssociatedWithPattern = await businessCardsRepo.getNumberOfUserCardsAssociatedWithPattern(userId, plainBusinessCard.PatternId);
            console.log("numberOfCardsAssociated = ", numberOfCardsAssociatedWithPattern);

            // if former current BC pattern is being used in other BC's then it cannot be unsubscribed
            if(Number(numberOfCardsAssociatedWithPattern) === 1){
                console.log("unsubscribing old pattern");
                const stripeCustomer = await stripeCustomersRepo.getByUserId(userId);
                const plainStripeCustomer = stripeCustomer.get({plain: true});
                const rentedPatterns = await usersRepo.getUserWithPatterns(userId);

                await stripeSubscriptionHandler.unsubscribeOldPattern(plainBusinessCard, plainStripeCustomer, rentedPatterns, t);
            }

            const deletionResult = await businessCardsRepo.removeById(id, t);
            if(deletionResult){
                console.log("Business card has been removed");
                console.log("setOrUnsetTierSubscription");
                //todo it updates subscription only for user who is removing his card
                //todo so another users (former card / user contacts) will have their subscription updated when they add / remove contact / join room
                await t.commit();

                t2 = await sequelize.transaction();

                await stripeSubscriptionHandler.setOrUnsetTierSubscription(userId, t2);

                await t2.commit();

                res.json({
                    message: `Business card ${id} has been successfully removed`
                });
            } else {
                await t.rollback();
                await t2.rollback();
                return next({status: 400, error: `Business card has not been removed for some reason`});
            }
        } else {
            return next({status : 400, error : 'Business card does not belongs to user with id : '+userId+' therefore it cannot be deleted'});
        }
    }).catch(async err =>{
        if(t){
            await t.rollback();
        }

        if(t2){
            await t2.rollback();
        }
        return next({status : 400, error : err});
    })
});


module.exports = router;