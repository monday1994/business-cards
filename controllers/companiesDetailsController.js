const express = require('express');
let router = express.Router();
const util = require('util');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const companiesDetialsRepo = require('../database/repositories/companiesDetailsRepo');
const usersRepo = require('../database/repositories/usersRepo');
const requestValidator = require('./requestsValidators/companiesDetailsRequestsValidator');
const crypto = require('../controllers/extraModules/crypto');
const shortId = require('shortid');
const sequelize = require('../database/relationalDB').sequelize;
const EMT = require('../configuration/errorsMessagesTemplates');
const sendgridApi = require('../api/sendgridApi');
const multerFilesValidator = require('./requestsValidators/multerFilesValidator');
const config = require('../configuration/config');
const Promise = require('bluebird');
const async = require('async');
const _ = require('lodash');

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        userId : 3,
        name : 'some awesome company name',
        role : 'CTO',
        website : 'https://www.somewebsite.com',
        phone : '111-222-333',
        address : 'Bysina 32',
        NIP : '1111111111',
        REGON : '222222222222',
        KRS : '3333333333333',
        description : 'some super extra company description'
    }
*/
//todo 28.12.17 works perfectly, transaction added
router.post('/createCompanyDetails', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    let t;

    requestValidator.validateCreateCompanyDetails(req).then(async result => {

        let userId = req.body.userId;

        let newCompanyDetails = {
            name : req.body.name,
            role : req.body.role,
            website : req.body.website,
            phone : req.body.phone,
            address : req.body.address,
            NIP : req.body.NIP,
            REGON : req.body.REGON,
            KRS : req.body.KRS,
            description : req.body.description,
            UserId : userId
        };

        t = await sequelize.transaction();

        const [ user, createdCompanyDetails ] = await Promise.all([
            usersRepo.getById(userId),
            companiesDetialsRepo.create(newCompanyDetails, t)
        ]);

        await user.addCompaniesDetails(createdCompanyDetails, {transaction: t});
        await t.commit();

        res.json({
            createdCompanyDetails : createdCompanyDetails
        });
    }).catch(async err => {
        if(t){
            await t.rollback();
        }
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        userId : 2,
        id : 1,
        name : 'some awesome company name',
        role : 'CTO',
        website : 'https://www.somewebsite.com',
        phone : '111-222-333',
        address : 'Bysina 32',
        NIP : '1111111111',
        REGON : '222222222222',
        KRS : '3333333333333',
        description : 'some super extra company description'
    }
*/
//todo 28.12.17 works perfectly
router.post('/updateCompanyDetails', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateUpdateCompanyDetails(req).then(result => {

        let userId = req.body.userId;

        let companyDetailsToBeUpdate = {
            id : req.body.id,
            name : req.body.name,
            role : req.body.role,
            website : req.body.website,
            phone : req.body.phone,
            address : req.body.address,
            NIP : req.body.NIP,
            REGON : req.body.REGON,
            KRS : req.body.KRS,
            description : req.body.description,
        };

        companiesDetialsRepo.update(companyDetailsToBeUpdate, userId).then(updatedCompanyDetails => {
            res.json({
                updatedCompanyDetails : updatedCompanyDetails
            });
        }).catch(err => {
            return next({status : 400, error : err});
        })
    }).catch(err => {
        return next({status : 400, error : err});
    });
});


/*
    headers : {
    }
    query : {
        userId : 2,
        id : 1
    }
*/
//todo works properly 28.12.17
router.get('/getCompanyDetailsById', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    requestValidator.validateGetCompanyDetailsById(req).then(result => {
        let companyDetailsId = req.query.id;
        let userId = req.query.userId;

        companiesDetialsRepo.getById(companyDetailsId).then(companyDetails => {
            if(companyDetails.UserId === userId){
                res.json({
                    companyDetails : companyDetails
                });
            } else {
                usersRepo.getUserContacts(companyDetails.UserId).then(arrOfContacts => {

                    let doesUserIsInContacts = _.find(arrOfContacts, { UserId : userId});

                    if(doesUserIsInContacts){
                        res.json({
                           companyDetails : companyDetails
                        });
                    } else {
                        return next({status : 401, error : 'User with id : '+userId+' have no access to companyDetails with id : '+companyDetailsId});
                    }
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            }
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err =>{
        return next({status : 400, error : err});
    });
});

/*
    headers : {
    }
    body : {
        userId : 1,
        id : 1
    }
*/
//todo 16.04.18 works properly, transaction added
router.post('/deleteById', passport.authenticate('jwt', {session: false}), (req,res,next) => {

    let t;

    requestValidator.validateDeleteById(req).then(async result => {
        let id = req.body.id;
        let userId = req.body.userId;

        t = await sequelize.transaction();

        const destroyResult = await companiesDetialsRepo.removeById(id, userId, t);

        await t.commit();
        res.json({
            message : `Company details with id : ${id} has been deleted`,
            result : destroyResult
        });
    }).catch(async err =>{
        if(t){
            await t.rollback();
        }
        return next({status : 400, error : err});
    })
});


module.exports = router;