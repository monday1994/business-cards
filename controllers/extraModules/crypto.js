const crypto = require('crypto');
const key = Buffer.from(process.env.CRYPTO_KEY, "utf-8"); //32 characters long key

function encrypt(text, iv){
    return new Promise((resolve, reject) => {
        try{
            const iv_buffer = Buffer.from(iv);

            const cipher = crypto.createCipheriv('aes-256-ctr', key, iv_buffer);

            let result = cipher.update(text, 'utf8', 'hex');
            result += cipher.final('hex');

            resolve(result);
        } catch(err){
            console.log("error in encrypt");
            reject('string: '+text+', cannot be encrypted');
        }
    });
}

exports.encrypt = encrypt;

function decrypt(text, iv){
    return new Promise((resolve, reject) => {
        try{
            const iv_buffer = Buffer.from(iv);
            const decipher = crypto.createDecipheriv('aes-256-ctr', key, iv_buffer);

            let result = decipher.update(text,'hex','utf8');
            result += decipher.final('utf8');

            resolve(result);
        } catch(err){
            console.log("error in decrypt");
            reject('string: '+text+', cannot be decrypted');
        }
    });
}

exports.decrypt = decrypt;




