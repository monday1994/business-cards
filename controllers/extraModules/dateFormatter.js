exports.getDateInShortFormat = function(){
    var now = new Date();

    var formattedDate = undefined;

    var day = now.getDate();
    var month = (now.getMonth() + 1);

    if(day > 9){
        formattedDate = day +'/';
    } else {
        formattedDate = '0'+day+'/';
    }

    if(month > 9){
        formattedDate += month;
    } else {
        formattedDate += '0'+month;
    }

    return formattedDate;
};