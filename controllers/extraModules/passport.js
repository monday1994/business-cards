const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;
const usersRepo = require('./../../database/repositories/usersRepo');
const usersAuthsRepo = require('../../database/repositories/usersAuthsRepo');
const adminsRepo = require('./../../database/repositories/adminsRepo');
const jwt = require('jsonwebtoken');
const util = require('util');
const Promise = require('bluebird');
const config = require('../../configuration/config');

// Setup work and export for the JWT passport strategy
module.exports = (passport) => {
    const opts = {
        jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("jwt"),
        secretOrKey: process.env.JWT_AUTH_SECRET
    };

    passport.use('jwt', new JwtStrategy(opts, (jwt_payload, done) => {
        let id = jwt_payload.id;
        let email = jwt_payload.email;
        Promise.all([
            usersAuthsRepo.verifyEmailAndId(email, id),
            adminsRepo.verifyEmailAndId(email, id)
        ]).spread((userAuth, admin) => {
            if(userAuth){
                let data = {
                    email: userAuth.email,
                    id: userAuth.UserId,
                    role : 'user',
                    type : userAuth.type
                };
                done(null, data);
            } else if(admin){
                let data = {
                    email: admin.email,
                    id: admin.id,
                    role : 'admin'
                };
                done(null, data);
            } else {
                done({status: 401, error : `Unauthorized, token invalid`});
            }
        }).catch(err => {
            done({ status: 400, error: err });
        });
    }));
};

