const Regex = require('regex');

module.exports = {
    onlyAlphabetLetterRegex : /^[a-zA-ZĄĆĘŁŃÓŚŹŻąćęłńóśźż\s]+$/,
    onlyNumbersRegex : /^[0-9]+$/,
    emailRegex : /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/,
    birthdayRegex : /[0-9]{2}[/][0-9]{2}[/][0-9]{4}$/,
    coordinatesRegex : /^(\-?\d+(\.\d+)?).\s*(\-?\d+(\.\d+)?)$/,
    hashTagRegex : /^#\S+$/,
    allCharactersUsedInWriting : /^[a-zA-Z0-9ĄĆĘŁŃÓŚŹŻąćęłńóśźż!@#$*_/.\-,\s]+$/,
    polishPostalCode : /[0-9]{2}\-[0-9]{3}/,
    accessToken : /^[a-zA-Z0-9]+$/,
    alphaNumeric : /^[a-z0-9]+$/,
    polishPhoneNumber : /^\d{3}[-]\d{3}[-]\d{3}$/,
    shortId : /^[a-zA-Z0-9_\-\s]+$/,
    stripeToken : /tok_[a-zA-Z0-9]+$/,
    websiteRegex : /(http|https):\/\/www+\.[a-zA-Z0-9]+\.[a-zA-Z]/,
    digitsRegex : /^[0-9]+$/,
    hexadecimalColorRegex : /^#[A-F0-9]+$/,
    sourceIdRegex : /src_[a-zA-Z0-9]+$/,
    creditCardIdRegex : /card_[a-zA-Z0-9]+$/,
    customerIdRegex : /cus_[a-zA-Z0-9]+$/,
    subscriptionIdRegex : /sub_[a-zA-Z0-9]+$/,
    planIdRegex : /tok_[a-zA-Z0-9]+$/,
    subscriptionItemIdRegex : /si_[a-zA-Z0-9]+$/,
    eventIdRegex : /evt_[a-zA-Z0-9]+$/
};

