const stripeApi = require('../../api/stripeApi');
const stripeCustomersRepo = require('../../database/repositories/stripeCustomersRepo');
const patternsRepo = require('../../database/repositories/patternsRepo');
const usersRepo = require('../../database/repositories/usersRepo');
const businessCardsRepo = require('../../database/repositories/businessCardsRepo');
const config = require('../../configuration/config').config;

const _ = require('lodash');

const subscriptionTiersPlansIdentifiers = [
    config.other.stripe.first_tier_plan.identifier,
    config.other.stripe.second_tier_plan.identifier,
    config.other.stripe.third_tier_plan.identifier
];


const FIRST_TIER_PLAN_MIN_NUM_OF_CONTACTS = config.other.stripe.first_tier_plan.minNumOfContacts,
    FIRST_TIER_PLAN_MAX_NUM_OF_CONTACTS = config.other.stripe.first_tier_plan.maxNumOfContacts,
    SECOND_TIER_PLAN_MIN_NUM_OF_CONTACTS = config.other.stripe.second_tier_plan.minNumOfContacts,
    SECOND_TIER_PLAN_MAX_NUM_OF_CONTACTS = config.other.stripe.second_tier_plan.maxNumOfContacts,
    THIRD_TIER_PLAN_MIN_NUM_OF_CONTACTS = config.other.stripe.third_tier_plan.minNumOfContacts;

const getDbPlanIdentifier = async (userId) => {
    const numOfContacts = await businessCardsRepo.getNumberOfUserContacts(userId);
    const rentedPatterns = await usersRepo.getUserWithPatterns(userId);
    const plainRentedPatterns = rentedPatterns.get({plain:true});

    const dbPlansIdentifiers = plainRentedPatterns.Patterns.reduce((plansToBeReturn, pattern) => {
        if(pattern.planIdentifier !== null){
            plansToBeReturn.push({ planIdentifier: pattern.planIdentifier });
        }
        return plansToBeReturn;
    }, []);

    const tierPlanIdentifier = subscriptionPlanChooser(numOfContacts);

    if(tierPlanIdentifier){
        dbPlansIdentifiers.push({ planIdentifier: tierPlanIdentifier})
    }

    console.log("user plans which have to be subscribed = ", dbPlansIdentifiers);

    return dbPlansIdentifiers
};

exports.getDbPlanIdentifier = getDbPlanIdentifier;

exports.findPlansToSubscribeOrUnsubscribe = (apiSubscriptions, dbSubscriptions) => {
    let result = {
        areEqual : true,
        apiPlansToUnsubscribe: [],
        dbPlansToSubscribe: []
    };

    if(apiSubscriptions.length === dbSubscriptions.length){
        const apiPlansToUnsubscribe = apiSubscriptions.reduce((acc, apiSub) => {
            let doesApiSubExistInDb = _.find(dbSubscriptions, { planIdentifier: apiSub.planIdentifier });

            if(!doesApiSubExistInDb){
                acc.push(apiSub);
            }
            return acc;
        }, []);

        if(apiPlansToUnsubscribe.length){
            //search db plan to subscribe
            const dbPlansToSubscribe = dbSubscriptions.reduce((acc, dbSub) => {
                let doesDbSubExistInApi = _.find(apiSubscriptions, {planIdentifier: dbSub.planIdentifier});

                if(!doesDbSubExistInApi){
                    acc.push(dbSub);
                }
                return acc;
            }, []);

            result.areEqual = false;
            result.dbPlansToSubscribe = dbPlansToSubscribe;
            result.apiPlansToUnsubscribe = apiPlansToUnsubscribe;
        }
    } else if(apiSubscriptions.length > dbSubscriptions.length){
        result.apiPlansToUnsubscribe = apiSubscriptions.filter(apiSub => {
            let doesApiSubExistInDb = _.find(dbSubscriptions, { planIdentifier: apiSub.planIdentifier});
            if(!doesApiSubExistInDb){
                return apiSub
            }
        });

        result.areEqual = false;
    } else {
        // find db subscriptions to subscribe in stripe
        result.dbPlansToSubscribe = dbSubscriptions.filter(dbSub => {
            let doesDbSubExistInApi = _.find(apiSubscriptions, { planIdentifier: dbSub.planIdentifier});
            if(!doesDbSubExistInApi){
                return dbSub
            }
        });

        result.areEqual = false;
    }

    return result;
};

//helper function
const unsubscribeOldPattern = async (businessCard, plainStripeCustomer, rentedPatterns, transaction) => {

    //unsubscribe old
    const patternToBeUnsubscribe = await patternsRepo.getById(businessCard.PatternId);

    if(patternToBeUnsubscribe.price > 0){
        console.log("unsubscribing old pattern");

        if(plainStripeCustomer.subscriptionItems.length === 1){
            console.log("removing subscription");
            //remove subscription
            await stripeApi.removeSubscription(plainStripeCustomer.subscriptionIdentifier);

            plainStripeCustomer.subscriptionItems = [];
            plainStripeCustomer.subscriptionIdentifier = null;

        } else if(plainStripeCustomer.subscriptionItems.length > 1){
            //remove subscription item
            console.log("removing subscription item");
            const subscriptionItem = _.find(plainStripeCustomer.subscriptionItems,
                { planIdentifier: patternToBeUnsubscribe.planIdentifier });

            await stripeApi.removeSubscriptionItem(subscriptionItem.subscriptionItemIdentifier);

            _.remove(plainStripeCustomer.subscriptionItems, { subscriptionItemIdentifier: subscriptionItem.subscriptionItemIdentifier });
            console.log("updating stripe customer = ", plainStripeCustomer);
        }

        await stripeCustomersRepo.tUpdate(plainStripeCustomer, transaction);
        await rentedPatterns.removePattern(patternToBeUnsubscribe, {transaction});
    }
};

exports.unsubscribeOldPattern = unsubscribeOldPattern;

//helper function
const subscribeNewPattern = async (plainStripeCustomer, pattern, transaction) => {
    if(plainStripeCustomer.subscriptionIdentifier){
        // if user has subscription then add plan to it

        console.log("subscription exist, plan is being added to it");
        const subscriptionItemIdentifier = await stripeApi.addPlanToSubscription(
            plainStripeCustomer.subscriptionIdentifier, pattern.planIdentifier
        );
        plainStripeCustomer.subscriptionItems.push({
            subscriptionItemIdentifier,
            planIdentifier: pattern.planIdentifier
        });
    } else {
        // if user does not have subscription then add subscription and plan
        const { subscriptionIdentifier, subscriptionItemIdentifier } = await stripeApi
            .createSubscription(plainStripeCustomer.customerIdentifier, pattern.planIdentifier);

        plainStripeCustomer.subscriptionIdentifier = subscriptionIdentifier;
        plainStripeCustomer.subscriptionItems.push({
            subscriptionItemIdentifier,
            planIdentifier: pattern.planIdentifier
        });
    }
    await stripeCustomersRepo.tUpdate(plainStripeCustomer, transaction);
};

exports.subscribeNewPattern = subscribeNewPattern;

//helper function
const subscriptionPlanChooser = (numOfContacts) => {

    if(numOfContacts >= FIRST_TIER_PLAN_MIN_NUM_OF_CONTACTS && numOfContacts <= FIRST_TIER_PLAN_MAX_NUM_OF_CONTACTS){
        return config.other.stripe.first_tier_plan.identifier;
    } else if (numOfContacts >= SECOND_TIER_PLAN_MIN_NUM_OF_CONTACTS && numOfContacts <= SECOND_TIER_PLAN_MAX_NUM_OF_CONTACTS) {

        return config.other.stripe.second_tier_plan.identifier;
    } else if (numOfContacts >= THIRD_TIER_PLAN_MIN_NUM_OF_CONTACTS) {

        return config.other.stripe.third_tier_plan.identifier;
    } else {
        return false;
    }
};

exports.subscriptionPlanChooser = subscriptionPlanChooser;

//helper function
const setOrUnsetTierSubscription = async (userId, transaction) => {
    const numOfContacts = await businessCardsRepo.getNumberOfUserContacts(userId);
    console.log("userid = ", userId);
    console.log("num of contacts = ", numOfContacts);

    const planIdentifier = subscriptionPlanChooser(numOfContacts);
    console.log("planIdentifier = ", planIdentifier);
    const stripeCustomer = await stripeCustomersRepo.getByUserIdIfUserHasCreditCard(userId);
    const plainStripeCustomer = stripeCustomer.get({plain:true});

    if(planIdentifier){
        const hasPlanBeenAlreadySubscribed = plainStripeCustomer.subscriptionItems.find((item) => planIdentifier === item.planIdentifier);
        if(!hasPlanBeenAlreadySubscribed){
            console.log("plan has not been subscribed before");
            if(plainStripeCustomer.subscriptionIdentifier){

                console.log("subscription exist, plan is being added to it");
                const subscriptionItemIdentifier = await stripeApi.addPlanToSubscription(
                    plainStripeCustomer.subscriptionIdentifier, planIdentifier
                );
                plainStripeCustomer.subscriptionItems.push({
                    subscriptionItemIdentifier,
                    planIdentifier
                });

                console.log("unsubscribing old");
                await unsubscribeOldPlan(plainStripeCustomer, transaction);
                // if user has subscription then add plan to it
            } else {
                console.log("creating subscription");
                // if user does not have subscription then add subscription and plan
                const { subscriptionIdentifier, subscriptionItemIdentifier } = await stripeApi
                    .createSubscription(plainStripeCustomer.customerIdentifier, planIdentifier);

                plainStripeCustomer.subscriptionIdentifier = subscriptionIdentifier;
                plainStripeCustomer.subscriptionItems.push({
                    subscriptionItemIdentifier,
                    planIdentifier
                });
            }
            await stripeCustomersRepo.tUpdate(plainStripeCustomer, transaction);
        }
    } else {
        if(plainStripeCustomer.subscriptionIdentifier){
            console.log("unsubscribing old plan if exist");
            await unsubscribeOldPlan(plainStripeCustomer, transaction);
        }
    }
};

exports.setOrUnsetTierSubscription = setOrUnsetTierSubscription;

const unsubscribeOldPlan = async (plainStripeCustomer, transaction) => {
    let tierPlanSubscription = plainStripeCustomer.subscriptionItems.find((item) => {
        return subscriptionTiersPlansIdentifiers.includes(item.planIdentifier);
    });

    if(tierPlanSubscription){
        //unsubscribe plan or remove subscription
        if(plainStripeCustomer.subscriptionItems.length === 1){
            console.log("removing subscription");
            //remove subscription
            await stripeApi.removeSubscription(plainStripeCustomer.subscriptionIdentifier);

            plainStripeCustomer.subscriptionItems = [];
            plainStripeCustomer.subscriptionIdentifier = null;

        } else if(plainStripeCustomer.subscriptionItems.length > 1){
            //remove subscription item
            console.log("removing subscription item");

            await stripeApi.removeSubscriptionItem(tierPlanSubscription.subscriptionItemIdentifier);

            _.remove(plainStripeCustomer.subscriptionItems, { subscriptionItemIdentifier: tierPlanSubscription.subscriptionItemIdentifier });
            console.log("updating stripe customer = ", plainStripeCustomer);
        }

        await stripeCustomersRepo.tUpdate(plainStripeCustomer, transaction);
    }
};

exports.unsubscribeOldPlan = unsubscribeOldPlan;
