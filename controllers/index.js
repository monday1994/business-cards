const express = require('express');
let router = express.Router();
const passport = require('passport');
const usersRepo = require('../database/repositories/usersRepo');
const bluebird = require('bluebird');
const util = require('util');

const fs = require('fs');
const multer = require('multer');
const upload = multer({ errorHandling : 'automatic' });
const multerFilesValidator = require('./requestsValidators/multerFilesValidator');

const db = require('../database/relationalDB').db;
const sequelize = require('../database/relationalDB').sequelize;




router.get('/', function(req, res){
    res.render('index', { user: req.user });
});


router.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});

router.get('/charge', (req,res,next)=>{
    res.render('create-source-stripe');
});

router.post('/charge', (req,res,next)=>{
    console.log("req.body = ", req.body);
    res.json({msg:'ok'});
});

router.get('/test', async (req,res,next) => {

    const db = require('../database/relationalDB').db;
    const sequelize = require('../database/relationalDB').sequelize;

    let newUser = {
        name : 'test',
        lastname : 'test',
        email: 'testemail2@gmail.com'
    };

    let newNotification = {
        UserId : null,
        type : 'Type',
        message : 'some msg'
    }

    let newRoom = {
        name : 'testroom',
        pin : '1234',
        creator: null
    }

    try {

        let t = await sequelize.transaction();
        //console.log("t = ", t);

        await db.User.create(newUser, {transaction: t});
        await db.Room.create(newRoom, {transaction: t});
        await db.Notification.create(newNotification, {transaction: t});

        console.log("roolback kurwa");
        t.rollback();

        res.json('ok');

    } catch (err){
        console.log("err in result = ", err);
    }
});

router.get('/sockets', (req,res,next) => {

    console.log("token on server side = ", util.inspect(req.query.token));

    res.render('sockets_test.ejs', {
        serverUrl : process.env.server_url,
        token : req.query.token
    });
});


module.exports = router;