const express = require('express');
let router = express.Router();
const util = require('util');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const patternsRepo = require('../database/repositories/patternsRepo');
const usersRepo = require('../database/repositories/usersRepo');
const notificationsRepo = require('../database/repositories/notificationsRepo');
const requestValidator = require('./requestsValidators/patternsRequestsValidator');
const crypto = require('../controllers/extraModules/crypto');
const shortId = require('shortid');
const stripeApi = require('../api/stripeApi');
const sequelize = require('../database/relationalDB').sequelize;
const EMT = require('../configuration/errorsMessagesTemplates');
const sendgridApi = require('../api/sendgridApi');
const multer = require('multer');
const upload = multer({ errorHandling : 'automatic' });
const multerFilesValidator = require('./requestsValidators/multerFilesValidator');
const config = require('../configuration/config');
const fs = require('fs');
const paths = require('../configuration/paths');
const Promise = require('bluebird');
const filesManager = require('./extraModules/filesManager');
const async = require('async');
const sockets = require('../sockets/mainSocketsController');

/*
    headers : {
        Content-Type : application/json,
        Authorization : admin JWT accessToken
    }
    body : {
        name : 'some awesome pattern',
        price : 22.0,
        obvers : JSON pattern
        revers : JSON pattern,
        currency : 'usd'
    }
*/
//todo 07.04.18 works perfectly, stripe added and tested, transaction added
router.post('/createPattern', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    if(req.user.role === 'admin'){

        let t;

        requestValidator.validateCreatePattern(req).then(async result => {
            let newPattern = {
                name: req.body.name,
                price: req.body.price,
                obvers: req.body.obvers,
                revers: req.body.revers,
                currency: req.body.currency,
                planIdentifier: null
            };

            if(newPattern.price > 0){
                newPattern.planIdentifier = await stripeApi.createPlan(newPattern.name, newPattern.price, newPattern.currency);
            }

            t = await sequelize.transaction();

            Promise.all([
                patternsRepo.tCreate(newPattern, t),
                usersRepo.getAllIds()
            ]).spread((createdPattern, usersIds) => {

                let notification = {
                    UserId: null,
                    type: 'NEW_PATTERN',
                    message: 'New business card pattern available',
                    isNew: true,
                    isReadOut: false,
                    associatedData: {
                        newPattern: createdPattern.get({plain: true})
                    }
                };

                //creating and sending notifs
                async.each(usersIds, (currentUser, cb) => {

                    notification.UserId = currentUser.id;

                    if (sockets.connectedUsers[currentUser.id]) {
                        notification.isNew = false;
                        sockets.sendNotification(notification);
                        console.log("noti has been send");
                    }

                    notificationsRepo.tCreate(notification, t).then(result => {
                        console.log("noti has been created");
                        cb(null);
                    }).catch(err => {
                        return cb(err);
                    });
                }, async err => {
                    if (err) {
                        if(t){
                            await t.rollback();
                        }
                        return next({status: 400, error: err});
                    } else {
                        await t.commit();

                        res.json({
                            createdPattern: createdPattern
                        });
                    }
                });
            }).catch(async err => {
                if(t){
                    await t.rollback();
                }
                return next({status: 400, error: err});
            });
        }).catch(async err => {
            if(t){
                await t.rollback();
            }
            return next({status: 400, error: err});
        });
    } else {
        return next({status : 401, error : EMT.INVALID_TOKEN_ERROR_MESSAGE})
    }
});


/*
    headers : {
        Content-Type : application/json,
        Authorization : admin JWT accessToken
    }
    body : {
        id : 1,
        name : 'some awesome pattern',
        revers: JSON pattern,
        obvers: JSON pattern,
    }
*/
//todo 28.12.17 works perfectly, transaction added
router.post('/updatePattern', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    if(req.user.role === 'admin'){

        let t;

        requestValidator.validateUpdatePattern(req).then(async result => {

            let patternToBeUpdate = {
                id : req.body.id,
                name : req.body.name,
                obvers: req.body.obvers,
                revers: req.body.revers,
            };

            t = await sequelize.transaction();

            const updatedPattern = await patternsRepo.update(patternToBeUpdate, t);

            await t.commit();

            res.json({
                updatedPattern : updatedPattern
            });
        }).catch(async err => {
            if(t){
                await t.rollback();
            }
            return next({status : 400, error : err});
        });
    } else {
        return next({status : 401, error : EMT.INVALID_TOKEN_ERROR_MESSAGE})
    }
});

/*
    headers : {
    }
    query : {
        id : 1
    }
*/
//todo works properly 28.12.17
router.get('/getPatternById', (req,res,next) => {
    requestValidator.validateGetPatternById(req).then(result => {
        let id = req.query.id;

        patternsRepo.getById(id).then(pattern => {
            res.json({
                pattern : pattern
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err =>{
        return next({status : 400, error : err});
    })
});

/*
    headers : {
    }
    query : {
    }
*/
//todo works properly 28.12.17
router.get('/getAllPatterns', (req,res,next) => {
    patternsRepo.getAll().then(patterns => {
        res.json({
            patterns : patterns
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json
        Authorization : Admin JWT
    }
    body : {
        id : 1
    }
*/
//todo 07.04.18 works, stripe remove plan implemented, transactions added
router.post('/deleteById', passport.authenticate('jwt', {session: false}), (req,res,next) => {

    if(req.user.role === 'admin'){

        let t;

        requestValidator.validateDeleteById(req).then(async result => {
            let id = req.body.id;

            const pattern = await patternsRepo.getByIdWithAssociatedUsers(id);
            let plainPattern = pattern.get({plain : true});

            if(plainPattern.Users.length === 0){

                if(plainPattern.planIdentifier){
                    await stripeApi.removePlan(plainPattern.planIdentifier);
                    console.log("plan has been removed");
                }

                t = await sequelize.transaction();

                console.log("pattern have not planIdentifier");
                const result = await patternsRepo.removeById(id, t);

                await t.commit();

                res.json({
                    message : 'pattern with id : '+id+' has been deleted',
                    result : result
                });
            } else {
                return next({status : 409, error : 'Pattern is associated with users therefore it cannot be deleted'});
            }
        }).catch(async err =>{
            if(t){
                await t.rollback();
            }
            return next({status : 400, error : err});
        })
    } else {
        return next({status: 401, error : EMT.INVALID_TOKEN_ERROR_MESSAGE});
    }
});



module.exports = router;