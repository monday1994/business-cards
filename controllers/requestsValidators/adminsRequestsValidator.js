const regex = require('./../extraModules/regex');
const VP = require('../../configuration/validationParameters');
const util = require('util');
const headersValidator = require('./headersValidator');
const VMT = require('../../configuration/validationMessagesTemplates');

exports.validateCreateAdmin = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.onlyAlphabetLetterRegex).isLength({
            min : VP.NAME_MIN_LENGTH,
            max : VP.NAME_MAX_LENGTH
        });
        req.checkBody('lastname', VMT.INVALID_LASTNAME_MESSAGE).matches(regex.onlyAlphabetLetterRegex).isLength({
            min : VP.LASTNAME_MIN_LENGTH,
            max : VP.LASTNAME_MAX_LENGTH
        });
        req.checkBody('email', VMT.INVALID_EMAIL_MESSAGE).matches(regex.emailRegex).isLength({
            min : VP.EMAIL_MIN_LENGTH,
            max : VP.EMAIL_MAX_LENGTH
        });
        req.checkBody('phone', VMT.INVALID_PHONE_NUMBER).matches(regex.polishPhoneNumber);

        req.checkBody('location', VMT.INVALID_LOCATION).matches(regex.onlyAlphabetLetterRegex).isLength({
            min : VP.LOCATION_MIN_LENGTH,
            max : VP.LOCATION_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateRemoveAdmin = (req) => {
    return new Promise((resolve, reject) => {
        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('adminId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.body.adminId = Number(req.body.adminId);

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};