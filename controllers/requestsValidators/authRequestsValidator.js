const regex = require('./../extraModules/regex');
const VP = require('../../configuration/validationParameters');
const util = require('util');
const headersValidator = require('./headersValidator');
const VMT = require('../../configuration/validationMessagesTemplates');

exports.validateLogin = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject({
                status : 400,
                error : 'invalid header type, must be application/json'
            });
        }

        req.checkBody('email', VMT.INVALID_EMAIL_MESSAGE).matches(regex.emailRegex).isLength({min : VP.EMAIL_MIN_LENGTH, max : VP.EMAIL_MAX_LENGTH});
        req.checkBody('password', VMT.INVALID_PASSWORD_MESSAGE).matches(regex.alphaNumeric).isLength({
            min : VP.PASSWORD_MIN_LENGHT, max : VP.PASSWORD_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject({
                    status : 400,
                    error : util.inspect(result.array())
                })
            }

            resolve(true);
        });
    });
};

exports.validateRegistration = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject({
                status : 400,
                error : 'invalid header type, must be application/json'
            });
        }
        req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.onlyAlphabetLetterRegex).isLength({
            min : VP.NAME_MIN_LENGTH, max : VP.NAME_MAX_LENGTH
        });
        req.checkBody('lastname', VMT.INVALID_LASTNAME_MESSAGE).matches(regex.onlyAlphabetLetterRegex).isLength({
            min : VP.LASTNAME_MIN_LENGTH, max : VP.LASTNAME_MAX_LENGTH
        });
        req.checkBody('email', VMT.INVALID_EMAIL_MESSAGE).matches(regex.emailRegex).isLength({min : VP.EMAIL_MIN_LENGTH, max : VP.EMAIL_MAX_LENGTH});


        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject({
                    status : 400,
                    error : util.inspect(result.array())
                })
            }

            resolve(true);
        });
    });
};

exports.validateLinkedinRegistration = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject({
                status : 400,
                error : 'invalid header type, must be application/json'
            });
        }
        req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.onlyAlphabetLetterRegex).isLength({
            min : VP.NAME_MIN_LENGTH, max : VP.NAME_MAX_LENGTH
        });
        req.checkBody('lastname', VMT.INVALID_LASTNAME_MESSAGE).matches(regex.onlyAlphabetLetterRegex).isLength({
            min : VP.LASTNAME_MIN_LENGTH, max : VP.LASTNAME_MAX_LENGTH
        });
        req.checkBody('linkedinEmail', VMT.INVALID_EMAIL_MESSAGE).matches(regex.emailRegex).isLength({min : VP.EMAIL_MIN_LENGTH, max : VP.EMAIL_MAX_LENGTH});

        req.checkBody('linkedinId', VMT.INVALID_EXTERNAL_ID_MESSAGE).matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.EXTERNAL_ID_MIN_LENGTH,
            max : VP.EXTERNAL_ID_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject({
                    status : 400,
                    error : util.inspect(result.array())
                })
            }

            resolve(true);
        });
    });
};

exports.validateAddLinkedinAuth = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject({
                status : 400,
                error : 'invalid header type, must be application/json'
            });
        }
        req.checkBody('userId', VMT.INVALID_NAME_MESSAGE).isInt({min : 1});

        req.checkBody('linkedinEmail', VMT.INVALID_EMAIL_MESSAGE).matches(regex.emailRegex).isLength({min : VP.EMAIL_MIN_LENGTH, max : VP.EMAIL_MAX_LENGTH});

        req.checkBody('linkedinId', VMT.INVALID_EXTERNAL_ID_MESSAGE).matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.EXTERNAL_ID_MIN_LENGTH,
            max : VP.EXTERNAL_ID_MAX_LENGTH
        });

        req.body.userId = Number(req.body.userId);

        if(req.user.role === 'user' && (req.user.id !== req.body.userId)){
            console.log("reject");
            reject(`Access token does not belong to user with id ${req.body.userId}`)
        }

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject({
                    status : 400,
                    error : util.inspect(result.array())
                })
            }

            resolve(true);
        });
    });
};

exports.validateChangePassword = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }
        req.checkBody('userId', VMT.INVALID_ID_MESSAGE).isInt({min:1});
        req.body.userId = Number(req.body.userId);
        if(req.user.role === 'user' && (req.user.id !== req.body.userId)){
            reject(`Token does not belong to user with id: ${req.body.userId}`)
        }

        req.checkBody('userEmail', VMT.INVALID_EMAIL_MESSAGE).matches(regex.emailRegex).isLength({
            min : VP.EMAIL_MIN_LENGTH,
            max : VP.EMAIL_MAX_LENGTH
        });

        req.checkBody('oldPassword', VMT.INVALID_PASSWORD_MESSAGE).matches(regex.alphaNumeric).isLength({
            min : VP.PASSWORD_MIN_LENGHT,
            max : VP.PASSWORD_MAX_LENGTH
        });
        req.checkBody('newPassword', VMT.INVALID_PASSWORD_MESSAGE).matches(regex.alphaNumeric).isLength({
            min : VP.PASSWORD_MIN_LENGHT,
            max : VP.PASSWORD_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateForgotPassword = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject({
                status : 400,
                error : 'invalid header type, must be application/json'
            });
        }

        req.checkBody('email', VMT.INVALID_EMAIL_MESSAGE).matches(regex.emailRegex).isLength({min : VP.EMAIL_MIN_LENGTH, max : VP.EMAIL_MAX_LENGTH});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject({
                    status : 400,
                    error : util.inspect(result.array())
                });
            }

            resolve(true);
        });
    });
};

exports.validateResetPasswordToken = function(req){
    return new Promise((resolve, reject) => {

        req.checkParams('token', VMT.INVALID_RESET_PASSWORD_TOKEN).matches(regex.accessToken).isLength({
            min : VP.RESET_PASSWORD_TOKEN_MIN_LENGTH,
            max : VP.RESET_PASSWORD_TOKEN_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject({
                    status : 400,
                    error : util.inspect(result.array())
                });
            }

            resolve(true);
        });
    });
};