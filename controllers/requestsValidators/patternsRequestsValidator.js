const regex = require('./../extraModules/regex');
const VP = require('../../configuration/validationParameters');
const VMT = require('../../configuration/validationMessagesTemplates');
const util = require('util');
const headersValidator = require('./headersValidator');

exports.validateCreatePattern = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.onlyAlphabetLetterRegex).isLength({
            min : VP.NAME_MIN_LENGTH,
            max : VP.NAME_MAX_LENGTH
        });

        req.checkBody('price', VMT.INVALID_PRICE_MESSAGE).isFloat({min : 0.0});

        req.checkBody('obvers', 'Must be json').isJSON();

        req.checkBody('revers', 'Must be json').isJSON();

        req.checkBody('currency').isIn(VP.CURRENCIES_ARR);

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateUpdatePattern = function (req) {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.body.id = Number(req.body.id);

        if(req.body.name){
            req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.onlyAlphabetLetterRegex).isLength({
                min : VP.NAME_MIN_LENGTH,
                max : VP.NAME_MAX_LENGTH
            });
        }

        if(req.body.revers){
            req.checkBody('revers', 'Must be json').isJSON();
        }

        if(req.body.obvers){
            req.checkBody('obvers', 'Must be json').isJSON();
        }

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateGetPattern = function (req) {
    return new Promise((resolve, reject) => {

        req.checkQuery('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.query.id = Number(req.query.id);

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateGetPatternById = function (req) {
    return new Promise((resolve, reject) => {

        req.checkQuery('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.query.id = Number(req.query.id);

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateDeleteById = function (req) {
    return new Promise((resolve, reject) => {
        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.body.id = Number(req.body.id);

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};
