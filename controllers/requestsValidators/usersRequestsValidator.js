const regex = require('./../extraModules/regex');
const VP = require('../../configuration/validationParameters');
const VMT = require('../../configuration/validationMessagesTemplates');
const util = require('util');
const headersValidator = require('./headersValidator');

exports.validateCreateClient = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.onlyAlphabetLetterRegex).isLength({
            min : VP.NAME_MIN_LENGTH,
            max : VP.NAME_MAX_LENGTH
        });
        req.checkBody('lastname', VMT.INVALID_LASTNAME_MESSAGE).matches(regex.onlyAlphabetLetterRegex).isLength({
            min : VP.LASTNAME_MIN_LENGTH,
            max : VP.LASTNAME_MAX_LENGTH
        });
        req.checkBody('email', VMT.INVALID_EMAIL_MESSAGE).matches(regex.emailRegex).isLength({
            min : VP.EMAIL_MIN_LENGTH,
            max : VP.EMAIL_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateSetProfilePhoto = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkFormDataContentType(req)){
            reject('invalid header type, must be form-data');
        }



        req.checkBody('userId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.body.userId = Number(req.body.userId);

        if(req.user.role === 'user' && (req.user.id !== req.body.userId)){
            reject(`Access token does not belong to user with id ${req.body.userId}`)
        }

        req.checkBody('userEmail', VMT.INVALID_EMAIL_MESSAGE).matches(regex.emailRegex).isLength({
            min : VP.EMAIL_MIN_LENGTH,
            max : VP.EMAIL_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateGetProfilePhoto = function(req){
    return new Promise((resolve, reject) => {

        req.checkParams('userId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.params.userId = Number(req.params.userId);

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateAddContact = function (req) {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('userCardId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.body.userCardId = Number(req.body.userCardId);

        req.checkBody('contactCardId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.body.contactCardId = Number(req.body.contactCardId);

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateRemoveContact = function (req) {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('userCardId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.body.userCardId = Number(req.body.userCardId);

        req.checkBody('contactCardId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.body.contactCardId = Number(req.body.contactCardId);

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateGetById = function (req) {
    return new Promise((resolve, reject) => {

        req.checkQuery('userId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.query.userId = Number(req.query.userId);

        if(req.user.role === 'user' && (req.user.id !== req.query.userId)){
            reject(`Access token does not belong to user with id ${req.query.userId}`)
        }

        req.checkQuery('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.query.id = Number(req.query.id);

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateGetLinkedinId = function(req){
    return new Promise((resolve, reject) => {

        req.checkQuery('userId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.query.userId = Number(req.query.userId);

        if(req.user.role === 'user' && (req.user.id !== req.query.userId)){
            reject(`Access token does not belong to user with id ${req.query.userId}`)
        }

        req.checkQuery('contactUserId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.query.contactUserId = Number(req.query.contactUserId);

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateSetNotificationAsReadOut = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }
        req.checkBody('userId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.body.userId = Number(req.body.userId);

        if(req.user.role === 'user' && (req.user.id !== req.body.userId)){
            reject(`Access token does not belong to user with id ${req.body.userId}`)
        }

        req.checkBody('notificationId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.body.notificationId = Number(req.body.notificationId);

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateSetCreditCard = (req) => {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }
        req.checkBody('userId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.body.userId = Number(req.body.userId);

        if(req.user.role === 'user' && (req.user.id !== req.body.userId)){
            reject(`Access token does not belong to user with id ${req.body.userId}`)
        }

        req.checkBody('creditCardId', VMT.INVALID_STRIPE_ID_MESSAGE).matches(regex.creditCardIdRegex).isLength({
            min: VP.STRIPE_ID_MIN_LENGTH,
            max : VP.STRIPE_ID_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};


exports.validateRemoveCreditCard = (req) => {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }
        req.checkBody('userId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.body.userId = Number(req.body.userId);

        if(req.user.role === 'user' && (req.user.id !== req.body.userId)){
            reject(`Access token does not belong to user with id ${req.body.userId}`)
        }

        req.checkBody('creditCardId', VMT.INVALID_STRIPE_ID_MESSAGE).matches(regex.creditCardIdRegex).isLength({
            min: VP.STRIPE_ID_MIN_LENGTH,
            max : VP.STRIPE_ID_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateDeleteUserAccount = (req) => {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }
        req.checkBody('userId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.body.userId = Number(req.body.userId);

        if(req.user.role === 'user' && (req.user.id !== req.body.userId)){
            reject(`Access token does not belong to user with id ${req.body.userId}`)
        }

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateSynchronizeStripe = (req) => {
    return new Promise((resolve, reject) => {

        req.checkQuery('userId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.query.userId = Number(req.query.userId);

        if(req.user.role === 'user' && (req.user.id !== req.query.userId)){
            reject(`Access token does not belong to user with id ${req.query.userId}`)
        }

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};