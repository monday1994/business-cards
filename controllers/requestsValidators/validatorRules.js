const regex = require('../../systemConfigs/regex');
const VP = require('../../systemConfigs/validationParameters');
const util = require('util');

const roomCreationRules = {
    name : ['required', 'regex:'+regex.allCharactersUsedInWriting, 'min:'+VP.NAME_MIN_LENGTH, 'max:'+VP.NAME_MAX_LENGTH]
};

const messageRules = {
    senderId : ['required', 'integer'],
    senderNickName : ['required', 'regex:'+regex.allCharactersUsedInWriting, 'min:'+VP.NAME_MIN_LENGTH, 'max:'+VP.NAME_MAX_LENGTH],
    message : ['required', 'regex:'+regex.allCharactersUsedInWriting, 'min:'+VP.MESSAGE_MIN_LENGTH]
};

module.exports = {
    ROOM_CREATION_RULES : roomCreationRules,
    MESSAGE_RULES : messageRules
};

//todo tests below
/*
var Validator = require('validatorjs');
var validation = new Validator(data, roomCreationRules);

console.log("passes ? = "+validation.passes()); // true
console.log("fail ? = "+util.inspect(validation.errors));*/

