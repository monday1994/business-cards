const express = require('express');
let router = express.Router();
const util = require('util');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const businessCardsRepo = require('../database/repositories/businessCardsRepo');
const usersRepo = require('../database/repositories/usersRepo');
const roomsRepo = require('../database/repositories/roomsRepo');
const notificationsRepo = require('../database/repositories/notificationsRepo');
const requestValidator = require('./requestsValidators/roomsRequestsValidator');
const crypto = require('../controllers/extraModules/crypto');
const shortId = require('shortid');
const sequelize = require('../database/relationalDB').sequelize;
const EMT = require('../configuration/errorsMessagesTemplates');
const config = require('../configuration/config').config;
const Promise = require('bluebird');
const async = require('async');
const _ = require('lodash');
const arrays = require('./extraModules/arrays');
const sockets = require('../sockets/mainSocketsController');
const stripeCustomersRepo = require('../database/repositories/stripeCustomersRepo');
const stripeApi = require('../api/stripeApi');
const stripeSubscriptionHandler = require('./extraModules/stripeSubscriptionHandler');
/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        userId: 1,
        name : 'some awesome company name',
        pin : '1234',
        creatorBusinessCardId : 3
    }
*/
//todo 28.12.17 works perfectly
router.post('/createRoom', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateCreateRoom(req).then(result => {

        let userId = req.body.userId;
        let creatorBusinessCardId = req.body.creatorBusinessCardId;
        businessCardsRepo.getByIdWithAssociatedData(creatorBusinessCardId).then(card => {
            if(card.UserId === userId){
                let newRoom = {
                    name : req.body.name,
                    pin : req.body.pin,
                    creator : userId,
                    members : [{UserId : userId, BusinessCardId : creatorBusinessCardId}]
                };

                roomsRepo.create(newRoom).then(createdRoom=>{
                    res.json({
                        createdRoom : createdRoom
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            } else {
                return next({status : 400, error : 'Business card with id : '+creatorBusinessCardId+' does not belong to user with id : '+userId});
            }
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        userId: 1,
        id : 1,
        name : 'some awesome company name',
        pin : '1234'
    }
*/
//todo 28.12.17 works perfectly
router.post('/updateRoom', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateUpdateRoom(req).then(result => {

        let userId = req.body.userId;

        let roomToBeUpdate = {
            id : req.body.id,
            name : req.body.name,
            pin : req.body.pin
        };

        roomsRepo.update(roomToBeUpdate, userId).then(updatedRoom=>{
            res.json({
                updatedRoom : updatedRoom
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        userId : 1,
        roomId : 1,
        businessCardId = 2
        pin : '1234'
    }
*/
//todo 28.12.17 works perfectly - to obtain linkedinId please use users/getLinkedinId endpoint
//todo transactions added, to update subscriptions for users use endpoint /updateRoomMembersSubscriptions
router.post('/joinRoom', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    let t;

    requestValidator.validateJoinRoom(req).then(async result => {

        let { userId, businessCardId, roomId, pin } = req.body;

        const [ userCardWithContacts, userCardWithPattern, room ] = await Promise.all([
            businessCardsRepo.getCardWithContactsById(businessCardId),
            businessCardsRepo.getByIdWithAssociatedPatternAndCompanyDetails(businessCardId),
            roomsRepo.getById(roomId)
        ]);
        if(userCardWithContacts.UserId === userId){
            if(room.pin === pin){
                let plainRoom = room.get({plain:true});
                console.log(`members = ${plainRoom.members}`);
                let parsedMembers = plainRoom.members;

                let isUserAlreadyInRoom = _.find(parsedMembers, {UserId : userId});

                if(!isUserAlreadyInRoom){

                    let plainCard = userCardWithContacts.get({plain : true});
                    let arrOfUserContacts = plainCard.Contacts;

                    let userContacts = arrOfUserContacts.map((currentContact) => {
                        return currentContact.CardCard.ContactId;
                    });

                    let notification = {
                        UserId : null,
                        type : 'NEW_CONTACT',
                        message : "You've got new contact.",
                        isNew : true,
                        isReadOut : false,
                        associatedData : {
                            NewContactBusinessCard : null
                        }
                    };

                    t = await sequelize.transaction();

                    await Promise.all(parsedMembers.map(async currentMember => {

                        let doesMemberIsAlreadyInContactWithUser = _.find(userContacts, {ContactId : currentMember.BusinessCardId});

                        if(!doesMemberIsAlreadyInContactWithUser){
                            const memberBusinessCard = await businessCardsRepo.getByIdWithAssociatedPatternAndCompanyDetails(currentMember.BusinessCardId);

                            await Promise.all([
                                userCardWithContacts.addContacts([currentMember.BusinessCardId], {transaction: t}),
                                memberBusinessCard.addContacts(userCardWithContacts, {transaction: t})
                            ]);

                            let memberNotification = JSON.parse(JSON.stringify(notification));

                            //notification for user who is joining the room
                            notification.UserId = userId;
                            notification.associatedData.NewContactBusinessCard = memberBusinessCard.get({plain : true});

                            if(sockets.connectedUsers[userId]){
                                notification.isNew = false;
                                sockets.sendNotification(notification);
                                //send noti to request maker
                            }

                            //notification for user who is already in room and got new contact
                            memberNotification.UserId = currentMember.UserId;
                            memberNotification.associatedData.NewContactBusinessCard = userCardWithPattern.get({plain : true});

                            if(sockets.connectedUsers[currentMember.UserId]){
                                //send noti to member
                                memberNotification.isNew = false;
                                sockets.sendNotification(memberNotification);
                            }

                            await Promise.all([
                                notificationsRepo.tCreate(notification, t),
                                notificationsRepo.tCreate(memberNotification, t)
                            ]);
                        }
                    }));

                    parsedMembers.push({
                        UserId : userId,
                        BusinessCardId : businessCardId
                    });

                    room.members = parsedMembers;

                    const updatedRoom = await room.update(room.get({plain:true}));
                       // await setOrUnsetTierSubscription(userId);
                    await t.commit();

                    res.json({
                        room : updatedRoom
                    });
                } else {
                    return next({status : 400, error : 'User with id : '+userId+' have already joined room with id : '+roomId});
                }
            } else {
                return next({status : 401, error : 'Wrong pin'});
            }
        } else {
            return next({ status: 400, error: 'Card with id : ' + businessCardId + ' does not belong to user with id : ' + userId });
        }
    }).catch(async err => {
        if(t){
            await t.rollback();
        }

        return next({status : 400, error : err});
    });
});


/*
    headers : {
        Authorization : user JWT
    }
    query : {
        userId : 1,
        roomId : 2,
    }
*/
//todo 18.04.18 - tranasctions added, works perfectly
router.get('/updateRoomMembersSubscriptions', passport.authenticate('jwt', {session: false}), (req,res,next) => {

    let t;

    requestValidator.validateUpdateRoomMembersSubscriptions(req).then(async result => {
        let { userId, roomId } = req.query;

        const room = await roomsRepo.getById(roomId);

        let plainRoom = room.get({plain:true});

        let isUserAlreadyInRoom = _.find(plainRoom.members, {UserId : userId});
        if(isUserAlreadyInRoom){

            t = await sequelize.transaction();

            await Promise.all(plainRoom.members.map(async currentMember => {
                await stripeSubscriptionHandler.setOrUnsetTierSubscription(currentMember.UserId, t);
            }));

            await t.commit();

            res.json({
                message : `Subscriptions successfully updated`
            });
        } else {
            return next({ status : 401, error : 'Cannot fetch room with _id : '+roomId+' user with id : '+ userId +' is not room member'});
        }
    }).catch(async err =>{
        if(t){
            await t.rollback();
        }
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        userId : 1,
        roomId : 1,
    }
*/
//todo 28.12.17 works perfectly
router.post('/leaveRoom', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateLeaveRoom(req).then(result => {

        let userId = req.body.userId;
        let roomId = req.body.roomId;

        roomsRepo.getById(roomId).then(room => {

            let plainRoom = room.get({plain:true});

            let isUserInRoom = _.find(plainRoom.members, {UserId : userId});

            if(isUserInRoom){

                if(userId !== plainRoom.creator){
                    arrays.removeObjectByKey(plainRoom.members, {UserId : userId});

                    room.members = plainRoom.members;

                    room.update(room.get({plain:true})).then(updatedRoom => {
                        res.json({
                            message : 'User with id : '+userId+' has successfully left room with id : '+roomId
                        });
                    }).catch(err => {
                        return next({status : 400, error : err});
                    })
                } else {
                    return next({status: 409, error: `Creator cannot leave room, try to delete the room instead`});
                }

            } else {
                return next({status : 400, error : 'User with id : '+userId+' did not join room with id : '+roomId});
            }
        }).catch(err => {
            return next({status : 400, error : err});
        });

    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Authorization : user JWT
    }
    query : {
        id : 1
    }
*/
//todo works properly 28.12.17
router.get('/getRoomById', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    requestValidator.validateGetRoomById(req).then(result => {
        let roomId = req.query.id;
        let userId = req.query.userId;

        roomsRepo.getById(roomId).then(room => {

            let plainRoom = room.get({plain:true});
            let parsedMembers = JSON.parse(plainRoom.members);
            let isUserAlreadyInRoom = _.find(parsedMembers, {UserId : userId});
            if(isUserAlreadyInRoom){
                res.json({
                    room : plainRoom
                });
            } else {
                return next({ status : 401, error : 'Cannot fetch room with _id : '+roomId+' user with id : '+ userId +' is not room member'});
            }
        }).catch(err=>{
            return next({status : 400, error : err});
        })
    }).catch(err =>{
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        content-type : 'application/json'
        Authorization : user JWT
    }
    body : {
        userId : 1,
        id : 1
    }
*/
//todo 11.04.18 works properly
router.post('/deleteById', passport.authenticate('jwt', {session: false}), (req,res,next) => {

    requestValidator.validateDeleteById(req).then(result => {
        let id = req.body.id;
        let userId = req.body.userId;

        roomsRepo.getById(id).then(roomToBeRemoved => {
            if(roomToBeRemoved.creator === userId){
                roomsRepo.removeById(id).then(result => {
                    res.json({
                        message : 'Room with id : '+id+' has been deleted'
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                })
            } else {
                return next({status :401, error : 'Room can be delete only by it creator'});
            }
        }).catch(err => {
            return next({status : 400, error : err});
        });

    }).catch(err =>{
        return next({status : 400, error : err});
    })
});


module.exports = router;