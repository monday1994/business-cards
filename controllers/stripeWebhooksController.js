const express = require('express');
let router = express.Router();
const util = require('util');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const usersRepo = require('../database/repositories/usersRepo');
const usersAuthsRepo = require('../database/repositories/usersAuthsRepo');
const notificationsRepo = require('../database/repositories/notificationsRepo');
const sequelize = require('../database/relationalDB').sequelize;
const EMT = require('../configuration/errorsMessagesTemplates');
const sendgridApi = require('../api/sendgridApi');
const stripeApi = require('../api/stripeApi');
const stripeCustomersRepo = require('../database/repositories/stripeCustomersRepo');
const stripeSubscriptionHandler = require('./extraModules/stripeSubscriptionHandler');
const sockets = require('../sockets/mainSocketsController');
const config = require('../configuration/config').config;
const _ = require('lodash');

/*

*/
//todo should work but requires more tests
//todo if webhooks are not being send from stripe and you are working locally then check whether ngrok tunelling is on

router.post('/webhooks', async (req,res,next) => {
    "use strict";

    let signature = req.headers["stripe-signature"];
    let t;
    try {
        //below request confirms that event come from stripe
        let event = stripeApi.stripe.webhooks.constructEvent(req.body, signature, config.other.stripe.webhooks_secret);

        const { data: { object: { customer: customer } } } = event;

        const customerToBeUpdate = {
            customerIdentifier: customer
        };

        let stripeCustomer;
        switch(event.type){
            case 'account.application.deauthorized':
                //block user account

                //todo check this does not happen when user deletes account
                console.log("deauthorized in stripe = ", event);
                break;
            case 'charge.failed':
                //todo this path works properly
                // block user account
                t = await sequelize.transaction();
                console.log("charge failed, customer = ", customer);

                customerToBeUpdate.isChargeable = false;
                customerToBeUpdate.isBlocked = true;
                stripeCustomer = await stripeCustomersRepo.updateAccountDueToWebhook(customerToBeUpdate, t);
                console.log(`customer to be blocked = ${stripeCustomer}`);
                if(stripeCustomer){
                    let notification = {
                        UserId: stripeCustomer.UserId,
                        type: 'CHARGE_FAILED',
                        message: `Your account has been blocked because you do not have enough founds on associated credit card`,
                        isNew: true,
                        isReadOut: false,
                        associatedData: null
                    };

                    if(sockets.connectedUsers[stripeCustomer.UserId]){
                        notification.isNew = false;
                        sockets.sendNotification(notification);
                    }

                    await notificationsRepo.tCreate(notification, t);
                }

                await t.commit();
                break;
            case 'charge.succeeded':
                //todo this path works properly
                //unblock account
                t = await sequelize.transaction();

                customerToBeUpdate.isChargeable = true;
                customerToBeUpdate.isBlocked = false;

                stripeCustomer = await stripeCustomersRepo.updateAccountDueToWebhook(customerToBeUpdate, t);

                if(stripeCustomer){
                    let notification = {
                        UserId: stripeCustomer.UserId,
                        type: 'CHARGE_ACCEPTED',
                        message: `Your account has been unblocked`,
                        isNew: true,
                        isReadOut: false,
                        associatedData: null
                    };

                    if(sockets.connectedUsers[stripeCustomer.UserId]){
                        notification.isNew = false;
                        sockets.sendNotification(notification);
                    }

                    await notificationsRepo.tCreate(notification, t);
                }

                await t.commit();
                break;
            case 'customer.source.expiring':
                //todo probably works test it
                stripeCustomer = await stripeCustomersRepo.getByCustomerIdentifier(customer);

                if(stripeCustomer){
                    let notification = {
                        UserId: stripeCustomer.UserId,
                        type: 'CREDIT_CARD_EXPIRING',
                        message: `Your credit card is going to expire next month. Please update the card.`,
                        isNew: true,
                        isReadOut: false,
                        associatedData: null
                    };

                    if(sockets.connectedUsers[stripeCustomer.UserId]){
                        notification.isNew = false;
                        sockets.sendNotification(notification);
                    }

                    await notificationsRepo.create(notification);
                }
                break;
        }

        res.json({
            result: true
        });
    }
    catch (err) {
        console.log("err in verification = ", err);
        return next({status: 400, error: err});
    }
});



module.exports = router;