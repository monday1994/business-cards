const express = require('express');
let router = express.Router();
const util = require('util');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const usersRepo = require('../database/repositories/usersRepo');
const usersAuthsRepo = require('../database/repositories/usersAuthsRepo');
const patternsRepo = require('../database/repositories/patternsRepo');
const businessCardsRepo = require('../database/repositories/businessCardsRepo');
const notificationsRepo = require('../database/repositories/notificationsRepo');
const requestValidator = require('./requestsValidators/usersRequestsValidator');
const crypto = require('../controllers/extraModules/crypto');
const shortId = require('shortid');
const sequelize = require('../database/relationalDB').sequelize;
const EMT = require('../configuration/errorsMessagesTemplates');
const sendgridApi = require('../api/sendgridApi');
const multer = require('multer');
const upload = multer({ errorHandling : 'automatic' });
const multerFilesValidator = require('./requestsValidators/multerFilesValidator');
const imageProcessor = require('./extraModules/imageProcessor');
const config = require('../configuration/config').config;
const fs = require('fs');
const paths = require('../configuration/paths');
const Promise = require('bluebird');
const filesManager = require('./extraModules/filesManager');
const stripeApi = require('../api/stripeApi');
const stripeCustomersRepo = require('../database/repositories/stripeCustomersRepo');
const stripeSubscriptionHandler = require('./extraModules/stripeSubscriptionHandler');
const s3Api = require('../api/s3Api');

const _ = require('lodash');

const subscriptionTiersPlansIdentifiers = [
    config.other.stripe.first_tier_plan.identifier,
    config.other.stripe.second_tier_plan.identifier,
    config.other.stripe.third_tier_plan.identifier
];

const FIRST_TIER_PLAN_MIN_NUM_OF_CONTACTS = config.other.stripe.first_tier_plan.minNumOfContacts,
      FIRST_TIER_PLAN_MAX_NUM_OF_CONTACTS = config.other.stripe.first_tier_plan.maxNumOfContacts,
      SECOND_TIER_PLAN_MIN_NUM_OF_CONTACTS = config.other.stripe.second_tier_plan.minNumOfContacts,
      SECOND_TIER_PLAN_MAX_NUM_OF_CONTACTS = config.other.stripe.second_tier_plan.maxNumOfContacts,
      THIRD_TIER_PLAN_MIN_NUM_OF_CONTACTS = config.other.stripe.third_tier_plan.minNumOfContacts;

/*
    headers : {
        Content-Type : multipart/form-data,
        Authorization : user JWT accessToken
    }
    body : {
        userId : 1,
        userEmail : 'user@gmail.com'
        file : 'profile picture in base64'
    }
*/
//todo works perfectly
router.post('/setProfilePhoto', upload.single('file'), passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    let t,
        newPhotoKey;

    requestValidator.validateSetProfilePhoto(req).then(async result => {
        let userId = req.body.userId;
        let userEmail = req.body.userEmail;
        let file = req.file;
        let xDimension = config.pictures.profilePictureXdimension;
        let yDimension = config.pictures.profilePictureYdimension;
        
        let validatedFile = await multerFilesValidator.validateFile(file);

        let fileToBeSave = await imageProcessor.imageMagickResize(validatedFile.buffer, xDimension, yDimension);
        let generatedFileName = shortId.generate();
        newPhotoKey = `${userId}_${generatedFileName}`;

        let linkToProfilePhoto = s3Api.createUrlToObjectInBucket(s3Api.users_photos_bucket, newPhotoKey);
        // upload to s3
        await s3Api.putPublicReadObjectIntoBucket(s3Api.users_photos_bucket, {key: newPhotoKey, value: fileToBeSave});    

        let userToBeUpdate = {
            id : userId,
            linkToProfilePhoto
        };

        let userFromDb = await usersRepo.getByEmail(userEmail);
        let plainUserFromDb = userFromDb.get({plain:true});

        t = await sequelize.transaction();

        let updatedUser = await usersRepo.tUpdate(userToBeUpdate, t);
        
        if(plainUserFromDb.linkToProfilePhoto){
            let photoToBeRemoveKey = s3Api.extractPhotoIdFromURL(plainUserFromDb.linkToProfilePhoto);

            await s3Api.deleteObjectFromBucket(s3Api.users_photos_bucket, photoToBeRemoveKey);
            await t.commit();

            res.json({
                linkToProfilePhoto
            });
        } else {
            await t.commit();

            res.json({
                linkToProfilePhoto : linkToProfilePhoto
            });
        };
    }).catch(async err => {
        if(t){
            await t.rollback();
            await s3Api.deleteObjectFromBucket(s3Api.users_photos_bucket, newPhotoKey);
        }
        return next({status: 400, error : err});
    });
});


/*
    headers : {
    }
    params : {
        userId : 1
    }
*/
//todo 12.05.18 - works perfectly
router.get('/getProfilePhoto/:userId', (req,res,next) => {
    requestValidator.validateGetProfilePhoto(req).then(async result => {
        let userId = req.params.userId;

        let user = await usersRepo.getById(userId);
        res.json({
            link: user.linkToProfilePhoto
        });
    }).catch(err =>{
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        userCardId : 1,
        contactCardId : 2
    }
*/
//todo 12.04.18 - works perfectly - to obtain linkedinId please use getLinkedinId endpoint, stripe implemented, transactions implemented
//
router.post('/addContact', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    let t;

    requestValidator.validateAddContact(req).then(async result => {

        let { userCardId, contactCardId } = req.body;

        const [ userCard, contactCard ] = await Promise.all([
            businessCardsRepo.getById(userCardId),
            businessCardsRepo.getById(contactCardId)
        ]);

        t = await sequelize.transaction();

        const [ firstResult, secondResult ] = await Promise.all([
                userCard.addContacts(contactCard, {transaction: t}),
                contactCard.addContacts(userCard, {transaction: t})
            ]);

        if(firstResult.length && secondResult.length){

            console.log("set or unset subscription");
            await stripeSubscriptionHandler.setOrUnsetTierSubscription(userCard.UserId, t);
            await stripeSubscriptionHandler.setOrUnsetTierSubscription(contactCard.UserId, t);

            await t.commit();
            res.json({
                message : 'Cards have been exchanged successfully'
            });
        } else {
            await t.rollback();

            res.json({
                message : 'Cards have not been exchanged, they probably have been exchanged before'
            });
        }
    }).catch(async err => {
        if(t){
            await t.rollback();
        }
        return next({status : 400, error : err})
    });
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        userCardId : 1,
        contactCardId : 2
    }
*/
//todo  06.04.18 works perfectly - to obtain linkedinId please use getLinkedinId endpoint, transaction implemented
//todo here is an issue when one of contacts have not chargeable card - then contacts cannot be removed
router.post('/removeContact', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    let t;

    requestValidator.validateRemoveContact(req).then(async result => {
        let { userCardId, contactCardId } = req.body;

        const [ userCard, contactCard ] = await Promise.all([
            businessCardsRepo.getById(userCardId),
            businessCardsRepo.getById(contactCardId)
        ]);

        t = await sequelize.transaction();

        const first = await contactCard.removeContacts(userCard, {transaction: t});
        const second = await userCard.removeContacts(contactCard, {transaction: t});

        if(first && second){

            console.log("set or unset subscription");
            await stripeSubscriptionHandler.setOrUnsetTierSubscription(userCard.UserId, t);
            await stripeSubscriptionHandler.setOrUnsetTierSubscription(contactCard.UserId, t);
            await t.commit();

            res.json({
                message : 'Contact have been successfully removed'
            });
        } else {

            await t.rollback();
            res.json({
                message : 'Contact have not been removed, it probably did not exist'
            });
        }
    }).catch(async err => {
        if(t){
            await t.rollback();
        }
        return next({status : 400, error : err})
    });
});

/*
    headers : {
        Authorization : user JWT accessToken
    }
    query : {
        userId : 1,
        id : 1,
    }
*/
//todo 27.12.17 - works perfectly
router.get('/getById', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateGetById(req).then(result => {
        let id = req.query.id;
        let userId = req.query.userId;

        if(id === userId){
            usersRepo.getById(id).then(user => {
                let plainUser = user.get({plain:true});

                res.json({
                    user : plainUser
                });
            }).catch(err => {
                return next({status : 400, error : err});
            });
        } else {
            usersRepo.getUserContacts(id).then(arrOfContacts => {

                let doesUserIsInContacts = _.find(arrOfContacts, { UserId : userId});

                if(doesUserIsInContacts){
                    usersRepo.getById(id).then(user => {
                        let plainUser = user.get({plain:true});
                        delete plainUser.password;

                        res.json({
                            user : plainUser
                        });
                    }).catch(err => {
                        return next({status : 400, error : err});
                    })
                } else {
                    return next({status : 401, error : 'User with id : '+userId+' have no access to user with id : '+id});
                }
            }).catch(err => {
                return next({status : 400, error : err});
            });
        }
    }).catch(err => {
        return next({status : 400, error : err})
    });
});

/*
    headers : {
        Authorization : user JWT accessToken
    }
    query : {
        userId : 1,
        //below is user which linkedin id is going to be fetched
        contactUserId : 1,
    }
*/
//todo 06.04.18 works
router.get('/getLinkedinId', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateGetLinkedinId(req).then(async result => {
        let { userId, contactUserId } = req.query;

        if(contactUserId === userId){
            try {
                const linkedinId = await usersAuthsRepo.getLinkedinAuthByUserId(contactUserId);

                res.json({
                    linkedinId
                });
            } catch(err){
                return next({status: 400, error: err});
            }
        } else {
            usersRepo.getUserContacts(contactUserId).then(async arrOfContacts => {

                let doesUserIsInContacts = _.find(arrOfContacts, { UserId : userId});

                if(doesUserIsInContacts){
                    try {
                        const linkedinId = await usersAuthsRepo.getLinkedinAuthByUserId(contactUserId);
                        res.json({
                            linkedinId
                        });
                    } catch(err){
                        return next({status: 400, error: err});
                    }
                } else {
                    return next({status : 401, error : 'User with id : '+userId+' have no access to user with id : '+contactUserId});
                }
            }).catch(err => {
                return next({status : 400, error : err});
            });
        }
    }).catch(err => {
        return next({status : 400, error : err})
    });
});


/*
    headers : {
        Content-Type : application/json
        Authorization : user JWT accessToken
    }
    body : {
        userId : 1,
        notificationId : 1,
    }
*/
router.post('/setNotificationAsReadOut',  passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateSetNotificationAsReadOut(req).then(result => {
        let notificationId = req.body.notificationId;
        let userId = req.body.userId;

        let notificationToBeUpdate = {
            id : notificationId,
            isReadOut : true
        };

        notificationsRepo.update(notificationToBeUpdate, userId).then(updatedNotification => {
            res.json({
                updatedNotification : updatedNotification
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({status : 400, error : err})
    });
});

/*
    headers : {
        Content-Type : application/json
        Authorization : user JWT accessToken
    }
    body : {
        userId : 1,
        creditCardId : 1,
    }
*/
//todo 11.04.18 works
router.post('/setCreditCard', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateSetCreditCard(req).then(async result => {
         const { userId, creditCardId } = req.body;

         const stripeCustomer = await stripeCustomersRepo.getByUserId(userId);
         const plainStripeCustomer = stripeCustomer.get({plain:true});

         await stripeApi.setDefaultSource(plainStripeCustomer.customerIdentifier, creditCardId);

         plainStripeCustomer.sourceIdentifier = creditCardId;

         await stripeCustomersRepo.update(plainStripeCustomer);

         res.json({
             message : `Credit card has been added to account : ${userId}`,
         });
    }).catch(err => {
        return next({status : 400, error : err})
    });
});

/*
    headers : {
        Content-Type : application/json
        Authorization : user JWT accessToken
    }
    body : {
        userId : 1,
        creditCardId : 1,
    }
*/
//todo 11.04.18 works
router.post('/removeCreditCard', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateRemoveCreditCard(req).then(async result => {
        const { userId, creditCardId } = req.body;

        const stripeCustomer = await stripeCustomersRepo.getByUserId(userId);
        const plainStripeCustomer = stripeCustomer.get({plain:true});

        if(stripeCustomer.subscriptionIdentifier){

            return next({status: 400, error: `User: ${userId} has active subscription, therefore card cannot be removed`});

        } else if (stripeCustomer.sourceIdentifier === creditCardId){
            await stripeApi.removeSource(plainStripeCustomer.customerIdentifier, plainStripeCustomer.sourceIdentifier);

            plainStripeCustomer.sourceIdentifier = null;

            await stripeCustomersRepo.update(plainStripeCustomer);

            res.json({
                message: `Credit card has been removed from user: ${userId} account`
            });
        } else {
            return next({status: 400, error : `Credit card id: ${creditCardId} does not match user cardId`});
        }

    }).catch(err => {
        return next({status : 400, error : err})
    });
});

/*
    headers : {
        Content-Type : application/json
        Authorization : user JWT accessToken
    }
    body : {
        userId : 1
    }
*/

//todo 17.04.18 works properly, has transaction and stripe implemented, add removing s3 user photos
router.post('/deleteUserAccount', passport.authenticate('jwt', {session: false}), async (req,res,next) => {
    "use strict";

    let t;

    requestValidator.validateDeleteUserAccount(req).then(async result => {
        const { userId } = req.body;

        t = await sequelize.transaction();

        await usersRepo.deleteUserAccount(userId, t);

        await t.commit();

        res.json({
            message: `User account with id: ${userId} has been successfully removed`
        });
    }).catch(async err => {
        if(t){
            await t.rollback();
        }
        return next({status : 400, error : err})
    });
});

// query = { userId : 1 }
//todo 18.04.18 works, transactions implemented
router.get('/synchronizeStripe', passport.authenticate('jwt', {session: false}), async (req,res,next) => {
    let t;

    requestValidator.validateSynchronizeStripe(req).then(async result => {
        const { userId } = req.query;

        const stripeCustomer = await stripeCustomersRepo.getByUserIdIfUserHasCreditCard(userId);
        const plainStripeCustomer = stripeCustomer.get({plain:true});

        const stripeApiCustomer = await stripeApi.getCustomerByIdentifier(plainStripeCustomer.customerIdentifier);

        if(plainStripeCustomer.subscriptionIdentifier && stripeApiCustomer.subscriptions.total_count > 0){

            const apiSubscriptions = stripeApiCustomer.subscriptions.data[0].items.data.map((currentSubscription) => {
                return {
                    planIdentifier : currentSubscription.plan.id,
                    subscriptionItemIdentifier : currentSubscription.id
                }
            });

            const dbPlansIdentifiers = await stripeSubscriptionHandler.getDbPlanIdentifier(userId);

            const { areEqual,
                dbPlansToSubscribe,
                apiPlansToUnsubscribe } = stripeSubscriptionHandler.findPlansToSubscribeOrUnsubscribe(apiSubscriptions, dbPlansIdentifiers);

            if(areEqual){
                console.log("nothing to synchronize");
                res.json({
                    synchronized: true,
                    numberOfSynchronizedSubscriptions: 0
                });
            } else if(dbPlansToSubscribe.length){
                console.log("subscribing new plans on stripe");
                t = await sequelize.transaction();

                const subscriptionsItems = await Promise.all(dbPlansToSubscribe.map(async planToSubscribe => {
                    let subscriptionItemIdentifier = await stripeApi.addPlanToSubscription(plainStripeCustomer.subscriptionIdentifier, planToSubscribe.planIdentifier);
                    return {
                        planIdentifier: planToSubscribe.planIdentifier,
                        subscriptionItemIdentifier
                    }
                }));

                plainStripeCustomer.subscriptionItems = [plainStripeCustomer.subscriptionItems, ...subscriptionsItems];

                await stripeCustomersRepo.tUpdate(plainStripeCustomer, t);

                await t.commit();

                res.json({
                    synchronized: true,
                    numberOfSynchronizedSubscriptions: dbPlansToSubscribe.length
                });
            } else {
                console.log("removing subscriptions from stripe");
                await Promise.all(apiPlansToUnsubscribe.map(async planToBeUnsubscribed => {
                    await stripeApi.removeSubscriptionItem(planToBeUnsubscribed.subscriptionItemIdentifier);
                    return 1;
                }));

                res.json({
                    synchronized: true,
                    numberOfSynchronizedSubscriptions: apiPlansToUnsubscribe.length
                });
            }
        } else {
            return next({status:400, error: `User: ${userId} does not have any subscription`});
        }
    }).catch(async err => {
        if(t){
            await t.rollback();
        }
        return next({status : 400, error : err})
    });
});


module.exports = router;