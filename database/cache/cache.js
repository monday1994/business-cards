const util = require('util');
const systemConstants = require('../../configuration/systemConstants');
const async = require('async');
const bluebird = require('bluebird');

const redis = require('redis');
//making redis promisify
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

//todo IT DOES NOT WORK, ITS ACTUALLY USELESS
//createClient('url_to_elasticcache')
// '//dusk-redis-cluster.lbtee4.0001.usw2.cache.amazonaws.com:6379'
//user:password@

const client = redis.createClient(process.env.REDIS_HOST_URL, (err) => {
    if(err){
        console.log("error while connecting to redis, err = "+util.inspect(err));
    }
});


client.on('connect', () => {
    console.log("Connected to redis server...");
});

client.on('end', () => {
    console.log("Connection with redis server has been closed");
});

client.on('reconnecting', () => {
    console.log("Connection with redis server has been interrupted. Reconnecting with redis...");
});



/*client.setAsync(1, JSON.stringify('dupa')).then((err, result)=>{
    if(err){
        return console.log(err);
    }
    console.log("message notifications has been saved for ");
});

client.get(1, (err, res)=>{
    console.log("res = "+util.inspect(res));
});*/








/*

//TODO jak cos nie dziala to cza sprawdzic czy roomId sie w bazie zgadza

exports.addMessage = function (roomId, message, callback) {
    delete message.receivers;
    var roomCacheId = 'room_messages:'+roomId;

    client.get(roomCacheId, function (err, messages) {
        if(err) return callback(err);
        var arr = [];

        if (messages) {
            arr = JSON.parse(messages);
            arr.push(message);
            if (arr.length > duskConstants.MAX_NUMBER_OF_MESSAGES_SAVED_IN_CACHE) {
                roomsRepo.addMessages(roomId, arr, function (err, result) {
                    if(err) {
                        console.error(err);
                        return callback(err);
                    }
                    client.set(roomCacheId, JSON.stringify([]), function(err, result){
                        if(err){
                            return callback(err);
                        }
                        console.log("cache has been added to mongodb in cache.addMessage");
                        callback(null, true);
                    });
                });
            } else {
                client.set(roomCacheId, JSON.stringify(arr), function(err, result) {
                    if (err) {
                        return callback(err);
                    }
                    callback(null, true);
                });
            }
        } else {
            arr = [message];
            client.set(roomCacheId, JSON.stringify(arr), function(err, result){
                if(err){
                    return callback(err);
                }
                callback(null, true);
            });
        }
    });
};

exports.removeRoomMessages = function(roomId, callback){
    var roomCacheId = 'room_messages:'+roomId;
    client.del(roomCacheId, function(err, result){
        if(err){
            return callback(err);
        }
        callback(null, result);
    });
};

//todo it should return objects
exports.getMessages = function (roomId, callback) {
    var roomCacheId = 'room_messages:'+roomId;
    client.get(roomCacheId, function (err, result) {
        if(err) return callback(err);

        var parsedArrayOfMessages = JSON.parse(result);

        callback(null, parsedArrayOfMessages);
    });
};


exports.addMessageNotification = function(roomId, receiverId, message , callback){

    delete message.receivers;

    var receiverCacheId = "message_notifications:"+receiverId;

    client.get(receiverCacheId, function(err, notifications){
        if(err) return callback(err);
        var arr = {};
        if(notifications){
            console.log("arr exists");
            arr = JSON.parse(notifications);
            arr[roomId] = message;

            client.set(receiverCacheId, JSON.stringify(arr), function(err, result){
                if(err){
                    return callback(err);
                }
                console.log("message notifications has been saved for "+receiverId);
                callback(null, true);
            });

        } else {
            console.log("arr doesn't exist");

            arr[roomId] = message;

            client.set(receiverCacheId, JSON.stringify(arr), function(err, result){
                if(err){
                    return callback(err);
                }
                console.log("message notifications has been saved for "+receiverId);
                callback(null, true);
            });
        }
    });
};


const getMessageNotifications = function(userId, callback){

    var notificationsToBeReturn = [];

    var userCacheId = "message_notifications:"+userId;
    client.get(userCacheId, function(err, notificationsFromCache){
        if(err) return callback(err);

        if(notificationsFromCache){
            var parsedNotifications = JSON.parse(notificationsFromCache);
            for(var elem in parsedNotifications){
                notificationsToBeReturn.push({
                    room_id : elem,
                    notification : parsedNotifications[elem]
                });
            }
            client.del(userCacheId, function(err, result){
                if(err) return callback(err);
                console.log("client.del in getMessageNotifications = "+result);
                return callback(null, notificationsToBeReturn);
            });
        } else {
            return callback(null, notificationsToBeReturn);
        }
    });
};

exports.getMessageNotifications = getMessageNotifications;

exports.getAllNotifications = function(userId, callback){

    var notifications = {
        messageNotifications : undefined,
        otherNotifications : []
    };

    async.parallel([
        function(callback){
            //todo tested and works properly 31.07.17
            getMessageNotifications(userId, function(err, arrayOfMessageNotifications){
                if(err) return callback(err);
                console.log("message notifs form cache = "+util.inspect(arrayOfMessageNotifications));
                notifications.messageNotifications = arrayOfMessageNotifications;
                callback(null, true);
            });
        },
        function(callback){

            getNotifications(userId, function(err, result){
                if(err) {
                    return callback(err);
                }
                if(result){
                    var parsed = JSON.parse(result);
                    var arrOfNotifications = parsed.arr;

                    async.each(arrOfNotifications, function(currentNotification, cb){
                        if(currentNotification.isReadOut === false){
                            notifications.otherNotifications.push(currentNotification);
                        }
                        cb(null);
                    }, function(err){
                        if(err) {
                            return callback(err);
                        }
                        callback(null, true);
                    });
                } else {
                    callback(null, 'no notifs');
                }

            });
        },
        function(callback){
            //todo should be tested
            notificationsRepo.getNotReadOutNotifs(userId, function(err, result){
                if(err) {
                    return callback(err);
                }
                console.log("notifs from db = "+util.inspect(result));
                if(result.length){
                    notifications.otherNotifications = notifications.otherNotifications.concat(result);
                }
                callback(null, true);
            });
        }
    ], function(err, result){
        if(err) return callback(err);
        console.log("notifications before return = "+util.inspect(notifications));
        callback(null, notifications);
    });
};


//todo below valid notification object
/!*{
    notifier : {type : String, required : true},
    type : {type : String, enum : ['activityComment','eventComment','eventJoin', 'activityJoin', 'roomLeave', 'hypeCalculationFactorOutOfDate'], required : true},
    identifier : {type : String},
    notificationMessage : {type : String, required : true}
    data : {type : object},
    isReadOut : 'false'
}*!/
//prefix = notifications
exports.addNotification = function(cacheOwnerId, notificationToBeCached, callback){

    var userId = "notifications:"+cacheOwnerId;
    client.get(userId, function(err, data){
        if(err) return callback(err);

        if(data){
            console.log("bucket exists");

            var bucket = JSON.parse(data);

            bucket.arr.push(notificationToBeCached);
            if(bucket.arr.length > duskConstants.MAX_NUMBER_OF_NOTIFICATIONS_SAVED_IN_CACHE){

                notificationsRepo.addNotifications(cacheOwnerId, bucket.arr, function(err, result){
                    if(err) {
                        console.error(err);
                        return callback(err);
                    }
                    console.log("notifs properly added to db");
                    bucket = { arr : [] };
                    client.set(userId, JSON.stringify(bucket), function(err, result){
                        if(err){
                            return callback(err);
                        }
                        callback(null, true);
                    });
                });
            } else {
                client.set(userId, JSON.stringify(bucket), function(err, result){
                    if(err){
                        return callback(err);
                    }
                    console.log("notifs saved in db");
                    callback(null, true);
                });
            }
        } else {
            console.log("bucket doesn't exist");

            bucket = { arr : [] };

            bucket.arr.push(notificationToBeCached);

            client.set(userId, JSON.stringify(bucket), function(err, result){
                if(err){
                    return callback(err);
                }
                console.log("notif saved in cache");
                callback(null, true);
            });
        }
    });
};

//todo it might be corrected to return objects, then check new implementation in getAllNotifications
const getNotifications = function(cacheOwnerId, callback){
    var userId = "notifications:"+cacheOwnerId;

    client.get(userId, function(err, cachedData){
        if(err) {
            return callback(err);
        }
        callback(null, cachedData);
    });
};

exports.getNotifications = getNotifications;


exports.markNotificationAsReadOut = function(cacheOwnerId, notifIdentifier, callback){
    var userId = "notifications:"+cacheOwnerId;
    var isNotificationChanged = false;
    client.get(userId, function(err, cachedData){
        if(err) {
            return callback(err);
        }
        var parsedData = JSON.parse(cachedData);

        async.each(parsedData.arr, function(currentNotification, cb){
            if(currentNotification.identifier === notifIdentifier){
                currentNotification.isReadOut = true;
                isNotificationChanged = true;
            }
            cb(null);
        }, function(err, result){
            if(err) {
                return callback(err);
            }
            if(isNotificationChanged){
                client.set(userId, JSON.stringify(parsedData), function(err, result){
                    if(err){
                        return callback(err);
                    }
                    callback(null, true);
                });
            } else {
                callback(null, false);
            }
        });
    });
};







//todo explore functions above

//todo ONLY TEST FUNCTION -> SHOULD BE REMOVED IN PRODUCTION
exports.removeAllData = function(){
    client.flushall();
};*/
