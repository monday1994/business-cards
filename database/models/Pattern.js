const VP = require('../../configuration/validationParameters');

module.exports = (sequelize, DataTypes) => {
    const Pattern = sequelize.define("Pattern", {
        name : {
            type : DataTypes.STRING
        },
        obvers : {
            type : DataTypes.JSON
        },
        revers : {
            type : DataTypes.JSON
        },
        price : {
            type : DataTypes.FLOAT
        },
        planIdentifier : {
            type : DataTypes.STRING
        },
        currency : {
            type : DataTypes.ENUM(VP.CURRENCIES_ARR)
        }
    }, {
        timestamps: false
    });

    return Pattern;
};