const VP = require('../../configuration/validationParameters');

module.exports = (sequelize, DataTypes) => {
    const Room = sequelize.define("Room", {
        name : {
            type : DataTypes.STRING
        },
        pin : {
            type : DataTypes.STRING
        },
        creator: {
            type : DataTypes.INTEGER
        },
        members : {
            type : DataTypes.JSON
        }
    }, {
        timestamps: false
    });

    return Room;
};