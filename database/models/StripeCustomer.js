const VP = require('../../configuration/validationParameters');

module.exports = (sequelize, DataTypes) => {
    const StripeCustomer = sequelize.define("StripeCustomer", {
        customerIdentifier : {
            type : DataTypes.STRING
        },
        sourceIdentifier : {
            type : DataTypes.STRING
        },
        subscriptionIdentifier: {
            type : DataTypes.STRING
        },
        //subscriptionItems structure -> [{ subscriptionItemIdentifier, planIdentifier }]
        subscriptionItems: {
            type : DataTypes.JSON
        },
        isBlocked : {
            type : DataTypes.BOOLEAN
        },
        isChargeable : {
            type : DataTypes.BOOLEAN
        }
    }, {
        timestamps: false
    });

    return StripeCustomer;
};