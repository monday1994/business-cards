const VP = require('../../configuration/validationParameters');

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define("User", {
        name : {
            type : DataTypes.STRING
        },
        lastname : {
            type : DataTypes.STRING
        },
        email: {
            type : DataTypes.STRING,
            unique : true
        },
        linkedinEmail: {
            type : DataTypes.STRING,
            unique : true
        },
        linkToProfilePhoto : {
            type : DataTypes.STRING
        },
    }, {
        timestamps: false
    });

    return User;
};