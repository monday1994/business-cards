const VP = require('../../configuration/validationParameters');

module.exports = (sequelize, DataTypes) => {
    const UserAuth = sequelize.define("UserAuth", {
        externalId: {
            type : DataTypes.STRING,
            unique : true
        },
        email: {
            type : DataTypes.STRING
        },
        password : {
            type : DataTypes.STRING
        },
        iv_string : {
            type : DataTypes.STRING
        },
        forgotPasswordToken : {
            type : DataTypes.STRING
        },
        forgotPasswordTokenExpiry : {
            type : DataTypes.BIGINT
        },
        type : {
            type : DataTypes.ENUM(VP.AUTH_TYPES)
        }
    }, {
        timestamps: false
    });

    return UserAuth;
};