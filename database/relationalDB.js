const util = require('util');
const Promise = require('bluebird');
const Sequelize = require('sequelize');
const config = require('../configuration/config').config.database;
//Sequelize.useCLS(transactionsNamespace);
//operators
const Op = Sequelize.Op;

const sequelize = new Sequelize(config.database, config.username, config.password, {
    dialect : config.dialect,
    host: config.host,
    port: process.db_port
}, {operatorAliases : false});


// Connect all the models/tables in the database to a db object,
//so everything is accessible via one object
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

const initEntities = function(){

    //Models/tables
    db.Admin = require('./models/Admin')(sequelize, Sequelize);
    db.BusinessCard = require('./models/BusinessCard')(sequelize, Sequelize);
    db.CompanyDetails = require('./models/CompanyDetails')(sequelize, Sequelize);
    db.Notification = require('./models/Notification')(sequelize, Sequelize);
    db.Pattern = require('./models/Pattern')(sequelize, Sequelize);
    db.Room = require('./models/Room')(sequelize, Sequelize);
    db.User = require('./models/User')(sequelize, Sequelize);
    db.UserAuth = require('./models/UserAuth')(sequelize, Sequelize);
    db.StripeCustomer = require('./models/StripeCustomer')(sequelize, Sequelize);

    return new Promise((resolve, reject) => {
        //creating relations
        Promise.all([
            // 1-M User - UserAuth
            db.User.hasMany(db.UserAuth, { as : 'UserAuths'}),
            db.UserAuth.belongsTo(db.User, { as : 'User'}),

            //todo test if both sides needed 1-1 User - StripeCustomer
            //db.User.hasOne(db.StripeCustomer, { as : 'StripeCustomer'}),
            db.StripeCustomer.belongsTo(db.User, { as : 'User'}),

            // 1-M User - BusinessCard
            db.User.hasMany(db.BusinessCard, { as : 'BusinessCards'}),
            db.BusinessCard.belongsTo(db.User, { as : 'User'}),

            // 1-M User - CompanyDetails
            db.User.hasMany(db.CompanyDetails, { as : 'CompaniesDetails'}),
            db.CompanyDetails.belongsTo(db.User, { as : 'User' }),

            // 1-1 BusinessCard - CompanyDetails
            db.BusinessCard.belongsTo(db.CompanyDetails, { as: 'CompanyDetails', foreignKey: 'CompanyDetailsId'}),
            db.CompanyDetails.hasOne(db.BusinessCard, { as: 'BusinessCard', foreignKey: 'CompanyDetailsId' }),

            // M:N BusinessCard - BusinessCard
            db.BusinessCard.belongsToMany(db.BusinessCard, {as : 'Contacts', through : {model : 'CardCard', unique : false}, foreignKey : 'BusinessCardId'}),

            // 1-M RentedPattern - BusinessCard
            db.Pattern.hasMany(db.BusinessCard, { as : 'BusinessCards'}),
            db.BusinessCard.belongsTo(db.Pattern, { as : 'Pattern'}),

            // M-N RentedPattern - User
            db.User.belongsToMany(db.Pattern, {as : 'Patterns', through : {model : 'UserPattern', unique : false}, foreignKey : 'UserId'}),
            db.Pattern.belongsToMany(db.User, {as : 'Users', through : {model : 'UserPattern', unique : false}, foreignKey : 'PatternId'}),

        ]).then((results)=>{
            //console.log("results db association = "+util.inspect(results));
            console.log("db has been initialized");
            resolve(results);
        }).catch(err => {
            console.error('db error = '+err);
            reject(err);
        });
    });
};

db.initDb = function(isForced){
    return new Promise((resolve, reject) => {
        initEntities().then((results)=>{
            db.sequelize.sync({force : false}).then(() => {
                console.log("db initialized");
                resolve('database initialized');
            }).catch(err => {
                console.error(err);
                reject(err);
            });
        }).catch(err=>{
            console.log("error occured when entities were initializing");
            reject(err);
        });
    });
};

module.exports = {
    db : db,
    sequelize : sequelize,
    Sequelize : Sequelize
};
