const util = require('util');
const db = require('../relationalDB').db;
const BusinessCard = db.BusinessCard;
const async = require('async');

exports.create = (newBusinessCard, transaction) => {
    return BusinessCard.create(newBusinessCard, {transaction});
};

exports.getByIdWithAssociatedPatternAndCompanyDetails = function(id){
    return new Promise((resolve, reject)=>{
        BusinessCard.findOne({where : {id : id}, include : [
                {
                    model : db.CompanyDetails, as : 'CompanyDetails'
                },
                {
                    model : db.Pattern, as : 'Pattern'
                }
            ]}).then(businessCardFromDb => {
            if(businessCardFromDb && businessCardFromDb.Pattern){
                resolve(businessCardFromDb);
            } else {
                reject('BusinessCard with id : '+id+' does not exist in db or does not have associated pattern');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getByIdWithCompanyDetailsAndPattern = function(id){
    return new Promise((resolve, reject)=>{
        BusinessCard.findOne({where : {id : id}, include : [
                {
                    model : db.CompanyDetails, as : 'CompanyDetails'
                },
                {
                    model : db.Pattern, as : 'Pattern'
                }
            ]}).then(businessCardFromDb => {
            if(businessCardFromDb){
                resolve(businessCardFromDb);
            } else {
                reject(`Business card with id ${id} does not exist in db`);
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getByIdWithAssociatedData = function(id){
    return new Promise((resolve, reject)=>{
        BusinessCard.findOne({where : {id : id}, include : [
                {
                    model : db.CompanyDetails, as : 'CompanyDetails'
                },
                {
                    model : db.Pattern, as : 'Pattern'
                },
                {
                    model : db.BusinessCard, as : 'Contacts'
                }
            ]}).then(businessCardFromDb => {
            if(businessCardFromDb){
                resolve(businessCardFromDb);
            } else {
                reject('BusinessCard with id : '+id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getById = function (id) {
    return new Promise((resolve, reject)=>{
        BusinessCard.findOne({where : {id : id}}).then(businessCardFromDb => {
            if(businessCardFromDb){
                resolve(businessCardFromDb);
            } else {
                reject('BusinessCard with id : '+id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getNumberOfUserCardsAssociatedWithPattern = async (userId, patternId) => {
    const businessCards = await BusinessCard.findAndCountAll({where : {UserId: userId, PatternId: patternId } });
    return businessCards.count;
};

const getCardWithContactsById = function(id){
    return new Promise((resolve, reject)=>{
        BusinessCard.findOne({where : {id : id}, include : [
                {
                    model : db.BusinessCard, as : 'Contacts', attributes : ['UserId'], through : {attributes : ['ContactId']}
                }
            ]}).then(businessCardFromDb => {
            if(businessCardFromDb){
                resolve(businessCardFromDb)
            } else {
                reject('BusinessCard with id : '+id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getCardWithContactsById = getCardWithContactsById;

exports.getNumberOfContactsByCardId = async (cardId) => {
      const businessCard = await getCardWithContactsById(cardId);
      const plainCard = businessCard.get({plain:true});

      return plainCard.Contacts.length;
};

exports.getNumberOfUserContacts = async (userId) => {
    const allCards = await BusinessCard.findAll({where : {UserId : userId}, include : [
            {
                model : db.BusinessCard, as : 'Contacts', attributes : ['UserId'], through : {attributes : ['ContactId']}
            }
        ]});

    const numOfContacts = allCards.reduce((numOfContacts, currentCard) => {
        const plainCurrentCard = currentCard.get({plain:true});
        console.log("plain current card = ", plainCurrentCard);
        numOfContacts += plainCurrentCard.Contacts.length;
        return numOfContacts;
    }, 0);
    console.log("all cards = ", numOfContacts);

    return numOfContacts;
};

exports.update = function(businessCardToBeUpdate){
    return new Promise((resolve, reject)=>{
        BusinessCard.findOne({where : {id : businessCardToBeUpdate.id, UserId : businessCardToBeUpdate.UserId}}).then(businessCardFromDb => {
            if(businessCardFromDb){
                businessCardFromDb.update(businessCardToBeUpdate).then(updatedBusinessCard => {
                    resolve(updatedBusinessCard);
                }).catch(err =>{
                    reject(err);
                })
            } else {
                reject(`business card with id : ${businessCardToBeUpdate.id} does not exist in db or does not belong to user with id : ${businessCardToBeUpdate.UserId}`);
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.removeById = (businessCardId, transaction) => {
    return BusinessCard.destroy({where : {id : businessCardId}, transaction})
};
