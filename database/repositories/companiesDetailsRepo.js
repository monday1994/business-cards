const util = require('util');
const db = require('../relationalDB').db;
const CompanyDetails = require('../relationalDB').db.CompanyDetails;


exports.create = (newCompanyDetails, transaction) => {
    return CompanyDetails.create(newCompanyDetails, {transaction});
};

exports.getById = function(id){
    return new Promise((resolve, reject)=>{
        CompanyDetails.findOne({where : {id : id}}).then(companyDetailsFromDb => {
            if(companyDetailsFromDb){
                resolve(companyDetailsFromDb);
            } else {
                reject('Company details with id : '+id+' do not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.update = async (companyDetailsToBeUpdate, ownerId) => {
    const companyDetailsFromDb = await CompanyDetails.findOne({where : {id : companyDetailsToBeUpdate.id}});

    if(companyDetailsFromDb){
        if(companyDetailsFromDb.UserId === ownerId){
            return companyDetailsFromDb.update(companyDetailsToBeUpdate);
        } else {
            throw('user with id : '+ownerId+' is not company details creator therefore cannot perform this operation');
        }
    } else {
        throw ('company details with id : '+companyDetailsToBeUpdate.id+' do not exist in db');
    }
};

exports.removeById = async (companyDetailsId, ownerId, transaction) => {

    const companyDetailsFromDb = await CompanyDetails.findOne({where : {id : companyDetailsId}, include : [
            {
                model : db.BusinessCard, as : 'BusinessCard'
            }
        ]});

    if(companyDetailsFromDb){
        if(companyDetailsFromDb.UserId === ownerId){

            if(companyDetailsFromDb.BusinessCard){
                throw (`Company details with id : ${companyDetailsId} is associated with business card, therefor it cannot be deleted`);
            } else {
                return CompanyDetails.destroy({where : {id : companyDetailsId}, transaction});
            }
        } else {
            throw ('user with id : '+ownerId+' is not company details creator therefore cannot perform this operation');
        }
    } else {
        throw (`Company details with id: ${companyDetailsId} does not exist in db`);
    }
};
