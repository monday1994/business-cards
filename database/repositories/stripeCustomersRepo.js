const util = require('util');
const db = require('../relationalDB').db;
const StripeCustomer = db.StripeCustomer;

exports.create = function(newStripeCustomer, transaction){
    newStripeCustomer.isBlocked = false;

    return StripeCustomer.create(newStripeCustomer, { transaction })
};

exports.update = async (stripeCustomerToBeUpdate) => {
    const stripeCustomerFromDb = await StripeCustomer.findOne({where : {UserId : stripeCustomerToBeUpdate.UserId}});

    if(stripeCustomerFromDb){
        return stripeCustomerFromDb.update(stripeCustomerToBeUpdate);
    } else {
        throw (`customer with userId : ${stripeCustomerToBeUpdate.UserId} does not exist in db`);
    }
};

exports.updateByCustomerIdentifier = async (stripeCustomerToBeUpdate) => {
    const stripeCustomerFromDb = await StripeCustomer.findOne({where : {customerIdentifier : stripeCustomerToBeUpdate.customerIdentifier}});

    if(stripeCustomerFromDb){
        return stripeCustomerFromDb.update(stripeCustomerToBeUpdate);
    } else {
        throw (`customer with userId : ${stripeCustomerToBeUpdate.UserId} does not exist in db`);
    }
};

exports.updateAccountDueToWebhook = async (stripeCustomerToBeUpdate, transaction) => {
    const stripeCustomerFromDb = await StripeCustomer.findOne({where : {customerIdentifier : stripeCustomerToBeUpdate.customerIdentifier}});

    if(stripeCustomerFromDb){
        if(stripeCustomerToBeUpdate.isBlocked !== stripeCustomerFromDb.isBlocked){
            console.log("stripeCustomerToBeUpdate before = ", stripeCustomerToBeUpdate);
            await stripeCustomerFromDb.update(stripeCustomerToBeUpdate, {transaction});
            return stripeCustomerFromDb.get({plain:true});
        } else {
            return false;
        }
    } else {
        throw (`customer with userId : ${stripeCustomerToBeUpdate.UserId} does not exist in db`);
    }
};

exports.tUpdate = async (stripeCustomerToBeUpdate, transaction) => {
    const stripeCustomerFromDb = await StripeCustomer.findOne({where : {UserId : stripeCustomerToBeUpdate.UserId}});

    if(stripeCustomerFromDb){
        return stripeCustomerFromDb.update(stripeCustomerToBeUpdate, {transaction});
    } else {
        throw (`customer with userId : ${stripeCustomerToBeUpdate.UserId} does not exist in db`);
    }
};

exports.getByCustomerIdentifier = async (customerIdentifier) => {
    const stripeCustomer = await StripeCustomer.findOne({where: {customerIdentifier}});

    if(stripeCustomer){
        return stripeCustomer
    } else {
        throw (`customer with identifier: ${customerIdentifier} does not exist in db`);
    }
};

exports.getByUserId = (userId) => {
    return StripeCustomer.findOne({where : {UserId : userId}});
};

exports.getByUserIdIfUserHasCreditCard = async (userId) => {
    const customer = await StripeCustomer.findOne({where: {UserId: userId}});

    if(customer && customer.sourceIdentifier){
        return customer;
    } else {
        throw (`User: ${userId} does not exist or does not have credit card associated`);
    }
};

exports.removeByUserId = function(userId){
    return StripeCustomer.destroy({where : {UserId : userId}});
};

