const util = require('util');
const db = require('../relationalDB').db;
const UserAuth = db.UserAuth;
const stripeApi = require('../../api/stripeApi');
const Promise = require('bluebird');

exports.create = function(newAuth, transaction){
    return UserAuth.create(newAuth, { transaction });
};

exports.createLinkedinAccount = async (newAuth, newUser, transaction) => {

    const createdUser = await db.User.create(newUser, { transaction });
    const createdAuth = await db.UserAuth.create(newAuth, {transaction});

    const customerIdentifier = await stripeApi.createCustomer(newUser.linkedinEmail, `${newUser.name} ${newUser.lastname}`);

    const newCustomer = {
        customerIdentifier,
        isBlocked : false,
        subscriptionItems: []
    };

    const createdCustomer = await db.StripeCustomer.create(newCustomer, {transaction});

    return {
        createdCustomer,
        createdUser,
        createdAuth
    };
};

exports.addLinkedinAccount = function(newAuth, userToBeUpdate, transaction){
    return db.User.findOne({where : {id : userToBeUpdate.id}}).then(userFromDb => {
        if(userFromDb && !userFromDb.linkedinEmail){
            return userFromDb.update(userToBeUpdate, {transaction}).then(updatedUser => {
                newAuth.UserId = updatedUser.id;
                return UserAuth.create(newAuth, {transaction})
            });
        } else {
            throw ('user with id : '+userToBeUpdate.id+' does not exist in db or have already linkedin account set up');
        }
    });
};

exports.update = function(authToBeUpdate){
    return new Promise((resolve, reject)=>{
        UserAuth.findOne({where : {UserId : authToBeUpdate.UserId}}).then(authFromDb => {
            if(authFromDb){
                authFromDb.update(authToBeUpdate).then(updatedAuth => {
                    resolve(updatedAuth);
                }).catch(err =>{
                    reject(err);
                })
            } else {
                reject('auth with UserId : '+authToBeUpdate.UserId+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.tUpdate = async (authToBeUpdate, transaction) => {
    const authFromDb = await UserAuth.findOne({where : {UserId : authToBeUpdate.UserId}});

    if(authFromDb){
        return authFromDb.update(authToBeUpdate, {transaction});
    } else {
        throw ('auth with UserId : '+authToBeUpdate.UserId+' does not exist in db');
    }
};

exports.removeByUserId = function(userId){
    return new Promise((resolve, reject) => {
        UserAuth.destroy({where : {UserId : userId}}).then(result => {
            resolve(result);
        }).catch(err => {
            reject(err);
        })
    });
};

exports.verifyEmailAndId = function(email, userId){
    return new Promise((resolve,reject) => {
        UserAuth.findOne({where: {email : email, UserId : userId}}).then((auth)=>{
            if(auth){
                resolve(auth);
            } else {
                resolve(null);
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.validateLogin = function(email, password){
    return new Promise((resolve, reject) => {
        UserAuth.findOne({where: {email : email, password : password}}).then((auth)=>{
            if(auth){
                resolve(auth);
            } else {
                reject('user does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.validateOldPassword = function(email, oldPassword){
    return new Promise((resolve, reject) => {
        UserAuth.findOne({where: {email : email, password : oldPassword}}).then((auth)=>{
            if(auth){
                resolve(auth);
            } else {
                reject('wrong email or password');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.getByForgotPasswordToken = function(token){
    return new Promise((resolve, reject) => {
        UserAuth.findOne({where: {forgotPasswordToken : token, type : 'STANDARD'}}).then((auth)=>{
            if(auth){
                let now = new Date().getTime();

                if(auth.forgotPasswordTokenExpiry < now){
                    reject('reset token expired');
                } else {
                    resolve(auth);
                }
            } else {
                reject('auth with token : '+token+' does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.getStandardAuthByEmail = function(email){
    return new Promise((resolve, reject) => {
        UserAuth.findOne({where: {email : email, type : 'STANDARD'}}).then((auth)=>{
            if(auth){
                resolve(auth);
            } else {
                reject('auth with email : '+email+' does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.getLinkedinAuthByUserId = async (userId) => {
    let userAuth = await UserAuth.findOne({where : { UserId: userId, type: 'LINKEDIN' }});

    if(userAuth){
        let { externalId } = userAuth.get({plain:true});
        return externalId;
    } else {
        return null;
    }
};

/*
usinessCard.findOne({where : {id : id}, include : [
        {
            model : db.BusinessCard, as : 'Contacts', attributes : ['UserId'], through : {attributes : ['ContactId']}
        }
    ]}).then(businessCardFromDb => {*/
