const util = require('util');
const db = require('../relationalDB').db;
const sequelize = require('../relationalDB').sequelize;
const stripeApi = require('../../api/stripeApi');
const User = db.User;

exports.create = function(newUser, transaction){
    return User.create(newUser, { transaction });
};

exports.update = function(userToBeUpdate){
    return new Promise((resolve, reject)=>{
        User.findOne({where : {id : userToBeUpdate.id}}).then(userFromDb => {
            if(userFromDb){
                userFromDb.update(userToBeUpdate).then(updatedUser => {
                    resolve(updatedUser);
                }).catch(err =>{
                    reject(err);
                })
            } else {
                reject('user with id : '+userToBeUpdate.id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.tUpdate = async (userToBeUpdate, transaction) => {
    const userFromDb = await User.findOne({where : {id : userToBeUpdate.id}});
    
    if(userFromDb){
        return userFromDb.update(userToBeUpdate, {transaction});
    } else {
        throw ('user with id : '+userToBeUpdate.id+' does not exist in db');
    }
};


exports.removeById = function(userId){
    return new Promise((resolve, reject) => {
       User.destroy({where : {id : userId}}).then(result => {
           resolve(result);
       }).catch(err => {
           reject(err);
       })
    });
};

exports.getAllIds = function(){
    return new Promise((resolve,reject)=>{
        User.findAll({attributes : ['id']}).then((users)=>{
            resolve(users);
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.getByEmail = async (email) => {
    let user = await  User.findOne({where: {email : email}});
    if(user){
        return user;
    } else {
        throw ('user with email : '+email+' does not exist in db');
    }
};

exports.getById = function(id){
    return new Promise((resolve,reject)=>{
        User.findOne({where: {id : id}}).then((user)=>{
            if(user){
                resolve(user);
            } else {
                reject('user with id : '+id+' does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.getUserWithPatterns = function(userId){
    return new Promise((resolve,reject)=>{
        User.findOne({where: {id : userId}, include : [
                {
                    model : db.Pattern, as : 'Patterns'
                }
            ], attributes : ['id', 'name', 'lastname']}).then((user)=>{
            if(user){
                resolve(user);
            } else {
                reject('user with id : '+id+' does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.getUserContacts = function(userId){
    return new Promise((resolve,reject)=>{
        User.findOne({where: {id : userId}, include : [
                {
                    model : db.BusinessCard, as : 'BusinessCards', include : [
                        {
                            model : db.BusinessCard, as : 'Contacts', attributes : ['UserId'], through : {attributes : []}
                        }
                    ]
                }
            ], attributes : ['id', 'name', 'lastname', 'email']}).then((user)=>{
            if(user){

                let dataToBeReturn = [];

                if(user.BusinessCards.length){
                    user.BusinessCards.forEach((currentCard, i)=>{
                        dataToBeReturn = dataToBeReturn.concat(currentCard.Contacts);
                        if(user.BusinessCards.length - 1 === i){
                            resolve(dataToBeReturn);
                        }
                    });
                } else {
                    resolve(false);
                }
            } else {
                reject('user with id : '+userId+' does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.deleteUserAccount = async (userId, t) => {
//todo remove photos from s3
    //rooms
    //notifications
    //company details
    // business cards
    // stripeCustomer
    // userAuth
    // remove photos from s3
    // user

        console.log("starting");
        await db.Room.destroy({where : { creator: userId }, transaction: t});
            console.log("rooms destroyed");
        await db.Notification.destroy({where :{ UserId: userId } , transaction: t});

        await db.CompanyDetails.destroy({where :{ UserId: userId },  transaction: t });
                    console.log("company details removed");
        await db.BusinessCard.destroy({where : { UserId: userId},  transaction: t });
                        console.log("BC destroyed");
        const stripeCustomer = await db.StripeCustomer.findOne({where :{ UserId: userId }});
                            console.log("stripe customer found ");

        await db.StripeCustomer.destroy({where :{ UserId: userId }, transaction: t});
        console.log("SC destroyed");

        await db.UserAuth.destroy({where :{ UserId: userId }, transaction: t});
        console.log("user auth destroyed ");

        await db.User.destroy({where :{ id: userId },  transaction: t});

        const { deleted } = await stripeApi.removeCustomer(stripeCustomer.customerIdentifier);
        console.log("stripe account delete result = ", deleted);
        if(!deleted){
            throw (`Something went wrong while user was being removed from stripe`);
        }

        return true;
};
