const util = require('util');

exports.handleError = (err, req, res, next) => {
    console.log("err in handleError = ", util.inspect(err));
    if(err.error.name && err.error.name.startsWith('Sequelize')){
        err.error = err.error.errors[0].message;
    }

    if(err.error.type && err.error.type.startsWith('Stripe')){
        err.status = err.error.statusCode;
        err.error = err.error.message;
    }

    res.status(err.status || 500).json({
        error : err.error
    });
};