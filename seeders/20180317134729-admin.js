'use strict';
const crypto = require('../controllers/extraModules/crypto');
const cryptoRandomString = require('crypto-random-string');

module.exports = {
  up: async (queryInterface, Sequelize) => {

      let iv_string = cryptoRandomString(16);

      const encryptedPassword = await crypto.encrypt('admin', iv_string);

      return queryInterface.bulkInsert('Admins', [
          {
            name : 'admin',
            lastname: 'admin',
            email: 'admin@gmail.com',
            // password = admin
            password : encryptedPassword,
            iv_string: iv_string,
            phone : '111-222-333',
            location : 'admin',
            forgotPasswordToken : null,
            forgotPasswordTokenExpiry : null,
      }], {});
  },
  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Admins', null, {});
  }
};
