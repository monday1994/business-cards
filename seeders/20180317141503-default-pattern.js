'use strict';
require('dotenv');
module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('Patterns', [
          {
              name : 'default_pattern',
              price : 0,
              obvers : JSON.stringify('default'),
              revers : JSON.stringify('default'),
              currency : 'usd',
              planIdentifier : null
          }], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Patterns', null, {});
  }
};
