const util = require('util');
const usersRepo = require('../database/repositories/usersRepo');
const notificationsRepo = require('../database/repositories/notificationsRepo');
const async = require('async');
const socketioJwt = require('socketio-jwt');
const connectedUsers = {};

exports.connectionHandler = function(io) {

    io.use(socketioJwt.authorize({
        secret : process.env.JWT_AUTH_SECRET,
        handshake : true
    }));

    io.on('connection', function (socket) {
        console.log("decoded token = "+util.inspect(socket.decoded_token));
        let userId = socket.decoded_token.id;
        usersRepo.getById(userId).then(user => {
            if(user){
                //ok
                connectedUsers[userId] = socket;

                console.log("user "+userId+" has been connected");

                sendNewNotifications(userId);
            } else {
                socket.emit('exception', {error : 'user with id: '+userId+' does not exist in db'});
            }
        });
        // function should be invoked always when internet connection is turned off
        socket.on("disconnect", function () {
            console.log("user disconnected");
            delete connectedUsers[socket.decoded_token.id];
        });
    });
};

exports.sendNotification = function(newNotification){
    connectedUsers[newNotification.UserId].emit('notifications', {
        notifications : [newNotification]
    });
};

const sendNewNotifications = function(userId){
    notificationsRepo.getAllNewNotifs(userId).then(notificationsToBeSend => {
        if(notificationsToBeSend.length){
            //set all as - isNew = false
            async.each(notificationsToBeSend, (currentNotification, cb) => {
                currentNotification.isNew = false;
                currentNotification.update(currentNotification.get({plain:true})).then(result => {
                    cb(null);
                }).catch(err=>{
                    return cb(err);
                });
            }, err=>{
                if(err){
                    connectedUsers[userId].emit('exception', {error : err});
                } else {
                    console.log("notifications set as not new");
                    connectedUsers[userId].emit('notifications', {
                        notifications : notificationsToBeSend
                    });
                }
            });
        } else {
            console.log("there was no notifs to be sent");
        }
    }).catch(err => {
        connectedUsers[userId].emit('exception', { error : err });
    })
};

exports.connectedUsers = connectedUsers;